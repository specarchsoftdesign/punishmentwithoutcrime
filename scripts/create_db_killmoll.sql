CREATE SCHEMA `KILLMOLL`;
USE `KILLMOLL`;

drop table if exists PHOTO;

/*==============================================================*/
/* Table: PHOTO                                                 */
/*==============================================================*/
create table PHOTO
(
   PHOTO_ID             bigint not null auto_increment,
   PHOTO_1              varchar(200),
   PHOTO_2              varchar(200),
   PHOTO_3              varchar(200),
   primary key (PHOTO_ID)
);

alter table PHOTO comment 'ФОТОГРАФИИ';

drop table if exists INANIMATE;

/*==============================================================*/
/* Table: INANIMATE                                             */
/*==============================================================*/
create table INANIMATE
(
   INANIMATE_ID         bigint not null auto_increment,
   NAME                 varchar(200) comment 'НАЗВАНИЕ НЕОДУШЕВЛЕННОГО ПРЕДМЕТА',
   DESCRIPTION          TEXT(50000) comment 'ОПИСАНИЕ НЕОДУШЕВЛЕННОГО ПРЕДМЕТА',
   PHOTO_ID             bigint,
   primary key (INANIMATE_ID)
);

alter table INANIMATE comment 'НЕОДУШЕВЛЕННЫЙ ПРЕДМЕТ';

alter table INANIMATE add constraint FK_INANIMAT_PHOTO foreign key (PHOTO_ID)
      references PHOTO (PHOTO_ID) on delete set null on update cascade;

drop table if exists ANIMAL;

/*==============================================================*/
/* Table: ANIMAL                                                */
/*==============================================================*/
create table ANIMAL
(
   ANIMAL_ID            bigint not null auto_increment,
   KIND                 varchar(100) comment 'ВИД ЖИВОТНОГО (СОБАКА, КОШКА И ПР.)',
   DESCRIPTION          TEXT(50000) comment 'ОПИСАНИЕ ЖИВОТНОГО',
   PHOTO_ID             bigint,
   primary key (ANIMAL_ID)
);

alter table ANIMAL comment 'ЖИВОТНОЕ';

alter table ANIMAL add constraint FK_ANIMAL_PHOTO foreign key (PHOTO_ID)
      references PHOTO (PHOTO_ID) on delete set null on update cascade;
 
 drop table if exists HUMAN;

/*==============================================================*/
/* Table: HUMAN                                                 */
/*==============================================================*/
create table HUMAN
(
   HUMAN_ID             bigint not null auto_increment,
   SECOND_NAME          varchar(50) comment 'ФАМИЛИЯ',
   FIRST_NAME           varchar(50) comment 'ИМЯ',
   PATHRONYMIC          varchar(50) comment 'ОТЧЕСТВО',
   GENDER               CHAR(1) comment 'ПОЛ',
   AGE                  int comment 'ВОЗРАСТ',
   GROWTH               smallint comment 'РОСТ',
   WEIGHT               smallint comment 'ВЕС',
   EYE_COLOR            varchar(30) comment 'ЦВЕТ ГЛАЗ',
   HAIR_COLOR           varchar(30) comment 'ЦВЕТ ВОЛОС',
   DESCRIPTION          TEXT(50000) comment 'ПОДРОБНОЕ ОПИСАНИЕ',
   PHOTO_ID             bigint,
   primary key (HUMAN_ID)
);

alter table HUMAN comment 'ЧЕЛОВЕК';

alter table HUMAN add constraint FK_HUMAN_PHOTO foreign key (PHOTO_ID)
      references PHOTO (PHOTO_ID) on delete set null on update cascade;

drop table if exists TYPE_OBJECT;

/*==============================================================*/
/* Table: TYPE_OBJECT                                           */
/*==============================================================*/
create table TYPE_OBJECT
(
   TYPE_OBJECT_ID       bigint not null auto_increment,
   NAME                 varchar(50) comment 'Название',
   AMOUNT               bigint comment 'Стоимость',   
   SURCHARGE            bigint comment 'Надбавка к стоимости за добавление собственных фотографий',
   primary key (TYPE_OBJECT_ID)
);

alter table TYPE_OBJECT comment 'ТИП ОБЪЕКТА ';

drop table if exists OBJECT;

/*==============================================================*/
/* Table: OBJECT                                                */
/*==============================================================*/
create table OBJECT
(
   OBJECT_ID            bigint not null auto_increment,
   TYPE_OBJECT_ID       bigint not null,
   INANIMATE_ID         bigint,
   ANIMAL_ID            bigint,
   HUMAN_ID             bigint,
   primary key (OBJECT_ID)
);

alter table OBJECT comment 'ОБЬЕКТ ЗАКАЗА';

alter table OBJECT add constraint FK_OBJECT_ANIMAL foreign key (ANIMAL_ID)
      references ANIMAL (ANIMAL_ID) on delete set null on update cascade;

alter table OBJECT add constraint FK_OBJECT_HUMAN foreign key (HUMAN_ID)
      references HUMAN (HUMAN_ID) on delete set null on update cascade;

alter table OBJECT add constraint FK_OBJECT_INANIMAT foreign key (INANIMATE_ID)
      references INANIMATE (INANIMATE_ID) on delete set null on update cascade;

alter table OBJECT add constraint FK_OBJECT_TYPE_OBJECT foreign key (TYPE_OBJECT_ID)
      references TYPE_OBJECT (TYPE_OBJECT_ID) on delete cascade on update cascade;

drop table if exists TYPE_STATUS;

/*==============================================================*/
/* Table: TYPE_STATUS                                           */
/*==============================================================*/
create table TYPE_STATUS
(
   TYPE_STATUS_ID       bigint not null,
   NAME                 varchar(50),
   primary key (TYPE_STATUS_ID)
);

alter table TYPE_STATUS comment 'СТАТУС ЗАКАЗА';

drop table if exists TYPE_DECORATION;

/*==============================================================*/
/* Table: TYPE_DECORATION                                       */
/*==============================================================*/
create table TYPE_DECORATION
(
   TYPE_DECORATION_ID   bigint not null auto_increment,
   NAME                 varchar(50),
   AMOUNT               bigint,
   SURCHARGE            bigint comment 'Надбавка к стоимости за добавление собственных фотографий',
   primary key (TYPE_DECORATION_ID)
);

alter table TYPE_DECORATION comment 'ВИД ОБСТАНОВКИ';

drop table if exists DECORATION;

/*==============================================================*/
/* Table: DECORATION                                            */
/*==============================================================*/
create table DECORATION
(
   DECORATION_ID        bigint not null auto_increment,
   TYPE_DECORATION_ID   bigint not null,
   DESCRIPTION          TEXT(50000),
   PHOTO_ID             bigint,
   primary key (DECORATION_ID)
);

alter table DECORATION comment 'ДЕКОРАЦИИ (ОБСТАНОВКА)';

alter table DECORATION add constraint FK_DECORATION_PHOTO foreign key (PHOTO_ID)
      references PHOTO (PHOTO_ID) on delete set null on update cascade;

alter table DECORATION add constraint FK_DECORATI_TYPE_DECORATION foreign key (TYPE_DECORATION_ID)
      references TYPE_DECORATION (TYPE_DECORATION_ID) on delete cascade on update cascade;

drop table if exists TYPE_WEAPON;

/*==============================================================*/
/* Table: TYPE_WEAPON                                           */
/*==============================================================*/
create table TYPE_WEAPON
(
   TYPE_WEAPON_ID       bigint not null auto_increment,
   NAME                 varchar(50),
   AMOUNT               bigint comment 'СТОИМОСТЬ',
   SURCHARGE            bigint comment 'Надбавка к стоимости за добавление собственных фотографий',
   primary key (TYPE_WEAPON_ID)
);

alter table TYPE_WEAPON comment 'ВИД ОРУДИЯ ПРЕСТУПЛЕНИЯ';

drop table if exists WEAPON;

/*==============================================================*/
/* Table: WEAPON                                                */
/*==============================================================*/
create table WEAPON
(
   WEAPON_ID            bigint not null auto_increment,
   TYPE_WEAPON_ID       bigint not null comment 'ТИП ОРУДИЯ ПРЕСТУПЛЕНИЯ',
   DESCRIPTION          TEXT(50000) comment 'ОПИСАНИЕ ОРУДИЯ ПРЕСТУПЛЕНИЯ',
   PHOTO_ID             bigint,
   primary key (WEAPON_ID)
);

alter table WEAPON comment 'ОРУДИЕ ПРЕСТУПЛЕНИЯ';

alter table WEAPON add constraint FK_WEAPON_PHOTO foreign key (PHOTO_ID)
      references PHOTO (PHOTO_ID) on delete set null on update cascade;

alter table WEAPON add constraint FK_WEAPON_TYPE_WEAPON foreign key (TYPE_WEAPON_ID)
      references TYPE_WEAPON (TYPE_WEAPON_ID) on delete cascade on update cascade;

drop table if exists ORDERS;

/*==============================================================*/
/* Table: ORDERS                                                */
/*==============================================================*/
create table ORDERS
(
   ORDER_ID             bigint not null auto_increment,
   TYPE_STATUS_ID       bigint not null,
   OBJECT_ID            bigint not null comment 'ОБЪЕКТ',
   WEAPON_ID            bigint not null comment 'ОРУДИЕ ПРЕСТУПЛЕНИЯ',
   DECORATION_ID        bigint not null comment 'ДЕКОРАЦИИ (ОБСТАНОВКА)',
   AMOUNT               bigint comment 'СТОИМОСТЬ',
   DATE_ORDER           date,
   DATE_EXECUTION       date,
   primary key (ORDER_ID)
);

alter table ORDERS comment 'ЗАКАЗ';

alter table ORDERS add constraint FK_ORDERS_DECORATION foreign key (DECORATION_ID)
      references DECORATION (DECORATION_ID) on delete cascade on update cascade;

alter table ORDERS add constraint FK_ORDERS_OBJECT foreign key (OBJECT_ID)
      references OBJECT (OBJECT_ID) on delete cascade on update cascade;

alter table ORDERS add constraint FK_ORDERS_TYPE_STATUS foreign key (TYPE_STATUS_ID)
      references TYPE_STATUS (TYPE_STATUS_ID) on delete cascade on update cascade;

alter table ORDERS add constraint FK_ORDERS_WEAPON foreign key (WEAPON_ID)
      references WEAPON (WEAPON_ID) on delete cascade on update cascade;

drop table if exists TYPE_ACCOUNT;

/*==============================================================*/
/* Table: TYPE_ACCOUNT                                          */
/*==============================================================*/
create table TYPE_ACCOUNT
(
   TYPE_ACCOUNT_ID      bigint not null,
   NAME                 varchar(50),
   primary key (TYPE_ACCOUNT_ID)
);

alter table TYPE_ACCOUNT comment 'ТИП АККАУНТА';

drop table if exists ACCOUNT;

/*==============================================================*/
/* Table: ACCOUNT                                               */
/*==============================================================*/
create table ACCOUNT
(
   ACCOUNT_ID           bigint not null auto_increment,
   TYPE_ACCOUNT_ID		bigint,
   LOGIN                varchar(20) not null,
   PASSWORD             varchar(100) not null,
   remember_token varchar(255),
   primary key (ACCOUNT_ID),
   unique key AK_AK_LOGIN (LOGIN)
);

alter table ACCOUNT comment 'АККАУНТ';

alter table ACCOUNT add constraint FK_ACCOUNT_TYPE_ACCOUNT foreign key (TYPE_ACCOUNT_ID)
      references TYPE_ACCOUNT (TYPE_ACCOUNT_ID) on delete restrict on update cascade;

drop table if exists CLIENT;

/*==============================================================*/
/* Table: CLIENT                                                */
/*==============================================================*/
create table CLIENT
(
   CLIENT_ID            bigint not null auto_increment,
   LAST_NAME            varchar(50),
   FIRST_NAME           varchar(50),
   PATHRONYMIC          varchar(50),
   BIRTH_DATE           date,
   PHONE_NUMBER         varchar(15) comment 'Номер телефона',
   ACCOUNT_ID           bigint not null,
   DATE_REGISTRATION    date comment 'ДАТА РЕГИСТРАЦИИ',
   primary key (CLIENT_ID)
);

alter table CLIENT comment 'КЛИЕНТ';

alter table CLIENT add constraint FK_CLIENT_ACCOUNT foreign key (ACCOUNT_ID)
      references ACCOUNT (ACCOUNT_ID) on delete cascade on update cascade;

drop table if exists CLIENT_ORDERS;

/*==============================================================*/
/* Table: CLIENT_ORDERS                                         */
/*==============================================================*/
create table CLIENT_ORDERS
(
   CLIENT_ORDER_ID      bigint not null auto_increment,
   CLIENT_ID            bigint,
   ORDER_ID             bigint,
   primary key (CLIENT_ORDER_ID)
);

alter table CLIENT_ORDERS add constraint FK_CLIENT_ORDERS_CLIENT foreign key (CLIENT_ID)
      references CLIENT (CLIENT_ID) on delete cascade on update cascade;

alter table CLIENT_ORDERS add constraint FK_CLIENT_ORDERS_ORDERS foreign key (ORDER_ID)
      references ORDERS (ORDER_ID) on delete cascade on update cascade;

drop table if exists POLICE_SETTING;

/*==============================================================*/
/* Table: POLICE_SETTING                                        */
/*==============================================================*/
create table POLICE_SETTING
(
   THRESHOLD_CRIME      int comment 'Порог преступлений (общий)',
   THRESHOLD_LEVEL_1    int comment 'Порог преступлений для первого уровня аккаунта',
   THRESHOLD_LEVEL_2    int comment 'Порог преступлений для второго уровня аккаунта',
   THRESHOLD_LEVEL_3    int comment 'Порог преступлений для третьего уровня аккаунта'
);

alter table POLICE_SETTING comment 'Настройки для правоохранительных органов';

