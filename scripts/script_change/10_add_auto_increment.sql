USE `KILLMOLL`; 
SET FOREIGN_KEY_CHECKS=0;
alter table type_weapon modify type_weapon_id bigint not null auto_increment;
alter table type_decoration modify type_decoration_id bigint not null auto_increment;
alter table type_object modify type_object_id bigint not null auto_increment;
alter table object modify object_id bigint not null auto_increment;
alter table human modify human_id bigint not null auto_increment;
alter table animal modify animal_id bigint not null auto_increment;
alter table inanimate modify inanimate_id bigint not null auto_increment;
alter table photo modify photo_id bigint not null auto_increment;
alter table weapon modify weapon_id bigint not null auto_increment;
alter table decoration modify decoration_id bigint not null auto_increment;
alter table orders modify order_id bigint not null auto_increment;
alter table client_orders modify client_order_id bigint not null auto_increment;
SET FOREIGN_KEY_CHECKS=1;
insert into dc_killmoll(dc_killmoll_id,description) values(10,'add_auto_increment');
commit;