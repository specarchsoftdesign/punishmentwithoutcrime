USE `KILLMOLL`;

/*==============================================================*/
/* Table: POLICE_SETTING                                        */
/*==============================================================*/
create table POLICE_SETTING
(
   THRESHOLD_CRIME      int comment 'Порог преступлений (общий)',
   THRESHOLD_LEVEL_1    int comment 'Порог преступлений для первого уровня аккаунта',
   THRESHOLD_LEVEL_2    int comment 'Порог преступлений для второго уровня аккаунта',
   THRESHOLD_LEVEL_3    int comment 'Порог преступлений для третьего уровня аккаунта'
);

alter table POLICE_SETTING comment 'Настройки для правоохранительных органов';

insert into dc_killmoll(dc_killmoll_id,description) values(7,'add_police_setting');
commit;