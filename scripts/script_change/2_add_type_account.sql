USE `KILLMOLL`;
drop table if exists TYPE_ACCOUNT;

/*==============================================================*/
/* Table: TYPE_ACCOUNT                                          */
/*==============================================================*/
create table TYPE_ACCOUNT
(
   TYPE_ACCOUNT_ID      bigint not null,
   NAME                 varchar(50),
   primary key (TYPE_ACCOUNT_ID)
);

alter table TYPE_ACCOUNT comment 'ТИП АККАУНТА';

alter table ACCOUNT add column TYPE_ACCOUNT_ID bigint;
alter table ACCOUNT add constraint FK_ACCOUNT_TYPE_ACCOUNT foreign key (TYPE_ACCOUNT_ID)
      references TYPE_ACCOUNT (TYPE_ACCOUNT_ID) on delete restrict on update cascade;

insert into dc_killmoll(dc_killmoll_id,description) values(2,'add_type_account');
commit;