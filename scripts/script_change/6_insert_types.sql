USE `KILLMOLL`; 
#Заполнение type_status
insert into type_status values(1,'в рассмотрении');
insert into type_status values(2,'одобрен');
insert into type_status values(3,'завершен');
insert into type_status values(4,'отменен');

#Заполнение type_object
insert into type_object(type_object_id,name,amount) values(1,'неодушевленный',5000);
insert into type_object(type_object_id,name,amount) values(2,'животное',20000);
insert into type_object(type_object_id,name,amount) values(3,'человек',100000);

#Заполнение type_weapon
insert into type_weapon(type_weapon_id,name,amount) values(1,'холодное',15000);
insert into type_weapon(type_weapon_id,name,amount) values(2,'огнестрельное',20000);
insert into type_weapon(type_weapon_id,name,amount) values(3,'метательное',10000);

#Заполнение type_decoration
insert into type_decoration(type_decoration_id,name,amount) values(1,'природа',15000);
insert into type_decoration(type_decoration_id,name,amount) values(2,'места общественного питания',20000);
insert into type_decoration(type_decoration_id,name,amount) values(3,'места культурного проведения досуга',30000);

insert into dc_killmoll(dc_killmoll_id,description) values(6,'insert_types');
commit;