USE `KILLMOLL`; 
alter table CLIENT drop foreign key FK_CLIENT_ACCOUNT;
alter table account modify account_id bigint not null auto_increment;
alter table CLIENT add constraint FK_CLIENT_ACCOUNT foreign key (ACCOUNT_ID)
      references ACCOUNT (ACCOUNT_ID) on delete cascade on update cascade;
      
alter table CLIENT_ORDERS drop foreign key FK_CLIENT_ORDERS_CLIENT;
alter table client modify client_id bigint not null auto_increment;     
alter table CLIENT_ORDERS add constraint FK_CLIENT_ORDERS_CLIENT foreign key (CLIENT_ID)
      references CLIENT (CLIENT_ID) on delete cascade on update cascade;
      
insert into dc_killmoll values(4,'add_increment_client_account');
commit;