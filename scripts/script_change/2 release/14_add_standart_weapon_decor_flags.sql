alter table killmoll.weapon add STANDART_WEAPON_FLAG smallint default 0 comment 'Флаг для типовых заказов';
alter table killmoll.decoration add STANDART_DECOR_FLAG smallint default 0 comment 'Флаг для типовых заказов';

insert into killmoll.dc_killmoll(dc_killmoll_id,description) values(14,'add_standart_weapon_decor_flags');
commit;