/*==============================================================*/
/* Table: COMPANY_SETTING                                       */
/*==============================================================*/
create table killmoll.COMPANY_SETTING
(
   COUNT_ORDER_LEVEL_2  smallint comment 'Количество заказов для перехода на второй уровень',
   COUNT_ORDER_LEVEL_3  smallint comment 'Количество заказов для перехода на третий уровень'
);

alter table killmoll.COMPANY_SETTING comment 'Настройки для компании';

insert into killmoll.dc_killmoll(dc_killmoll_id,description) values(13,'add_table_company_setting');
commit;