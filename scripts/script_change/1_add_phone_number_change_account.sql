USE `KILLMOLL`;
alter table client add column phone_number varchar(10);
alter table account modify login varchar(20);
alter table account modify password varchar(20);

insert into dc_killmoll(dc_killmoll_id,description) values(1,'add_phone_number_change_account');
commit;