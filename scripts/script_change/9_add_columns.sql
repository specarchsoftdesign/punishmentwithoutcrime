USE `KILLMOLL`; 
alter table type_weapon drop column img;
alter table type_weapon add SURCHARGE bigint comment 'Надбавка к стоимости за добавление собственных фотографий';

alter table type_decoration drop column img;
alter table type_decoration add SURCHARGE bigint comment 'Надбавка к стоимости за добавление собственных фотографий';

alter table type_object drop column img;
alter table type_object add SURCHARGE bigint comment 'Надбавка к стоимости за добавление собственных фотографий';

alter table client add CLIENT_LEVEL smallint comment 'Уровень клиента';

insert into dc_killmoll(dc_killmoll_id,description) values(9,'add_columns');
commit;