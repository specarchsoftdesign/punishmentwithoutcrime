﻿use `killmoll`;

#Добавление неодушевленных объектов для стандартных заказов
insert into inanimate(inanimate_id,name,description) 
	values(1,'Lamborghini Aventador','Спортивный автомоболь. Тип кузова- купе. 2 двери. Руль слева. Объем двигателя - 6498 куб.см. Мощность - 8250 об.мин. Тип привода - постоянный на все колеса. Тип КПП - механика 7 ст. Разгон до 100 км/час - 2,9 с. Длина - 4780 мм. Ширина - 2030 мм. Высота - 1136 мм. Колесная база - 2700 мм. Снаряженная масса - 1575 кг.');
insert into object(object_id,type_object_id,inanimate_id,standart_obj_flag) values(1,1,1,1);

insert into inanimate(inanimate_id,name,description) 
	values(2,'IPhone X','Длина - 143,6 мм. Ширина - 70,9 мм. Толщина - 7,7 мм. Вес 174 г.');
insert into object(object_id,type_object_id,inanimate_id,standart_obj_flag) values(2,1,2,1);

#Добавление животных объектов для стандартных заказов
insert into animal(animal_id,kind, description) values(1,'Кот','Черный дворовый кот. Хромает на заднюю правую лапу. Облезлый. Орет по утрам');
insert into object(object_id,type_object_id,animal_id,standart_obj_flag) values(3,2,1,1);

insert into animal(animal_id,kind, description) values(2,'Волк','Серенький волчок, который кусает за бочок. Рост в холке - 85 см. Длина туловища без учета звоста - 150 см. Вес - 70 кг.');
insert into object(object_id,type_object_id,animal_id,standart_obj_flag) values(4,2,2,1);

#Добавление людей-объектов для стандартных заказов
insert into human(human_id,second_name,first_name,pathronymic,gender,age,growth,weight,eye_color,hair_color,description)
	values(1,'Путин','Владимир','Владимирович','м',65,170,77,'светло-карие','русый','Президент РФ');
insert into object(object_id,type_object_id,human_id,standart_obj_flag) values(5,3,1,1);

insert into human(human_id,second_name,first_name,pathronymic,gender,age,growth,weight,eye_color,hair_color,description)
	values(2,'Бузова','Ольга','Игоревна','ж',31,176,53,'темно-карие','шатенка','Радиоведущая, модель, около певица, актриса');
insert into object(object_id,type_object_id,human_id,standart_obj_flag) values(6,3,2,1);
commit;