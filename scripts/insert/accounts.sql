﻿USE `KILLMOLL`;
#Заполнение резервных аккаунтов account
insert into account(account_id,type_account_id,login,password) values(1,2,'companykillmoll','$2y$10$pyPV8lcs38.bKprH1rRRauA3ngBx6RPrSuvNnZjEKDpmjTu8fG3Nm');
insert into account(account_id,type_account_id,login,password) values(2,3,'policekillmoll','$2y$10$OaTA3tfq3mHQfDtIhKNtRekmEpWNqedqWVERp.pe2gOndK/RwCC7.');
commit;