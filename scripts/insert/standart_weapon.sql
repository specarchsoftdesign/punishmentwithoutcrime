#стандартные оружия
insert into killmoll.weapon(weapon_id,type_weapon_id,description,standart_weapon_flag)
	values(1,1,'Нож. Длина лезвия 25 см, ширина 5 см. Крупная черная прорезиненная ручка',1);
insert into killmoll.weapon(weapon_id,type_weapon_id,description,standart_weapon_flag)
	values(2,2,'Автомат Калашникова - АК74. Патроны калибра 5,45 мм',1);
insert into killmoll.weapon(weapon_id,type_weapon_id,description,standart_weapon_flag)
	values(3,3,'Сюрикэн-звездочка',1);
commit;