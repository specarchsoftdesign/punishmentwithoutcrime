set sql_mode='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION,PIPES_AS_CONCAT';
﻿﻿#стандартные декорации
insert into killmoll.decoration(decoration_id,type_decoration_id,description,standart_decor_flag)
	values(1,1,'Хвойный лес. Ели закрывают своими густыми кронами небо, и плотно смыкаясь, '|| 
				'не пропускают солнечный свет. Под кронами еловых деревьев всегда царят полумрак, '||
                'влажность и прохлада. Чувствуется насыщенный запах хвои',1);
insert into killmoll.decoration(decoration_id,type_decoration_id,description,standart_decor_flag)
	values(2,1,'Дикий пляж на Черном море. Широкий песчаный берег, вдали виднеются густые заросли '|| 
			    'лиственных растений. Вода прозрачно-голубая настолько, что видно дно и видно маленьких '||
                'медуз, которые подплыли к берегу',1);

insert into killmoll.decoration(decoration_id,type_decoration_id,description,standart_decor_flag)
	values(3,2,'Самый дорогой ресторан Санкт-Петербурга - "Дом". В ресторане царит атмосфера '||
				'девятнадцатого века. Ресторан и правда похож на богатый дом девятнадцатого '||
                'века, в котором есть библиотека и даже винный погреб',1);
insert into killmoll.decoration(decoration_id,type_decoration_id,description,standart_decor_flag)
	values(4,2,'Макдональс. Пластмассовые столы и стулья красного цвета, длинный стол для заказов.'||
				'Вокруг много мониторов, на которых транслируется реклама',1);

insert into killmoll.decoration(decoration_id,type_decoration_id,description,standart_decor_flag)
	values(5,3,'Эрмитаж. Огромная зала с картинами Ван Гога, высокие сводчатые потолки',1);
insert into killmoll.decoration(decoration_id,type_decoration_id,description,standart_decor_flag)
	values(6,3,'Концертный зал. Многочисленные ряды синих бархатных кресел с позолоченными номерками '||
				'рядов. Огромная сцена, освещенная ярким светом софитов. На сцене много аппаратуры для '||
                'лазерного шоу',1);

commit;