use `killmoll`;

insert into photo(photo_id,photo_1) values(1,'storage/company/objects/lamborghini.jpg');
update inanimate set photo_id = 1 where inanimate_id = 1;

insert into photo(photo_id,photo_1) values(2,'storage/company/objects/iphone.jpg');
update inanimate set photo_id = 2 where inanimate_id = 2;

insert into photo(photo_id,photo_1) values(3,'storage/company/objects/cat.jpg');
update animal set photo_id = 3 where animal_id = 1;

insert into photo(photo_id,photo_1) values(4,'storage/company/objects/wolf.jpeg');
update animal set photo_id = 4 where animal_id = 2;

insert into photo(photo_id,photo_1) values(5,'storage/company/objects/putin.jpg');
update human set photo_id = 5 where human_id = 1;

insert into photo(photo_id,photo_1) values(6,'storage/company/objects/buzova.jpeg');
update human set photo_id = 6 where human_id = 2;

commit;