<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 14 Nov 2017 21:18:15 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class Account
 * 
 * @property int $account_id
 * @property string $login
 * @property string $password
 * @property int $TYPE_ACCOUNT_ID
 * 
 * @property \App\Models\TypeAccount $type_account
 * @property \Illuminate\Database\Eloquent\Collection $clients
 *
 * @package App\Models
 */
class Account extends Authenticatable
{
	protected $table = 'account';
	protected $primaryKey = 'account_id';
	public $incrementing = true;
	public $timestamps = false;

	protected $casts = [
		'TYPE_ACCOUNT_ID' => 'int'
	];

	protected $hidden = [
		'password'
	];

	protected $fillable = [
		'login',
		'password',
		'TYPE_ACCOUNT_ID'
	];

	public function type_account()
	{
		return $this->belongsTo(\App\Models\TypeAccount::class, 'TYPE_ACCOUNT_ID');
	}

	public function clients()
	{
		return $this->hasMany(\App\Models\Client::class, 'ACCOUNT_ID');
	}

	/*
	* Password need to be all time encrypted. 
	* 
	* @param string $password 
	*/ 
	public function setPasswordAttribute($password) 
	{ 
	$this->attributes['password'] = bcrypt($password); 
	}
}
