<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 14 Nov 2017 21:18:16 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Client
 * 
 * @property int $client_id
 * @property string $LAST_NAME
 * @property string $FIRST_NAME
 * @property string $PATHRONYMIC
 * @property \Carbon\Carbon $BIRTH_DATE
 * @property string $phone_number
 * @property int $ACCOUNT_ID
 * @property \Carbon\Carbon $DATE_REGISTRATION
 * @property int $CLIENT_LEVEL
 * 
 * @property \App\Models\Account $account
 * @property \Illuminate\Database\Eloquent\Collection $orders
 *
 * @package App\Models
 */
class Client extends Eloquent
{
	protected $table = 'client';
	protected $primaryKey = 'client_id';
	public $timestamps = false;

	protected $casts = [
		'ACCOUNT_ID' => 'int',
		'CLIENT_LEVEL' => 'int'
	];

	protected $dates = [
		'BIRTH_DATE',
		'DATE_REGISTRATION'
	];

	protected $fillable = [
		'LAST_NAME',
		'FIRST_NAME',
		'PATHRONYMIC',
		'BIRTH_DATE',
		'phone_number',
		'ACCOUNT_ID',
		'DATE_REGISTRATION',
		'CLIENT_LEVEL'
	];

	public function account()
	{
		return $this->belongsTo(\App\Models\Account::class, 'ACCOUNT_ID');
	}

	public function orders()
	{
		return $this->belongsToMany(\App\Models\Order::class, 'client_orders', 'CLIENT_ID', 'ORDER_ID')
					->withPivot('client_order_id');
	}
}
