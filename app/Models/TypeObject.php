<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 14 Nov 2017 21:18:16 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class TypeObject
 * 
 * @property int $type_object_id
 * @property string $NAME
 * @property int $AMOUNT
 * @property int $SURCHARGE
 * 
 * @property \Illuminate\Database\Eloquent\Collection $objects
 *
 * @package App\Models
 */
class TypeObject extends Eloquent
{
	protected $table = 'type_object';
	protected $primaryKey = 'type_object_id';
	public $timestamps = false;

	protected $casts = [
		'AMOUNT' => 'int',
		'SURCHARGE' => 'int'
	];

	protected $fillable = [
		'NAME',
		'AMOUNT',
		'SURCHARGE'
	];

	public function objects()
	{
		return $this->hasMany(\App\Models\Object::class, 'TYPE_OBJECT_ID', 'TYPE_OBJECT_ID');
	}
}
