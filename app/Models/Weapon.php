<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 14 Nov 2017 21:18:16 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Weapon
 * 
 * @property int $weapon_id
 * @property int $TYPE_WEAPON_ID
 * @property string $DESCRIPTION
 * @property int $PHOTO_ID
 * @property int $STANDART_WEAPON_FLAG
 * 
 * @property \App\Models\Photo $photo
 * @property \App\Models\TypeWeapon $type_weapon
 * @property \Illuminate\Database\Eloquent\Collection $orders
 *
 * @package App\Models
 */
class Weapon extends Eloquent
{
	protected $table = 'weapon';
	protected $primaryKey = 'weapon_id';
	public $timestamps = false;

	protected $casts = [
		'TYPE_WEAPON_ID' => 'int',
		'PHOTO_ID' => 'int',
		'STANDART_WEAPON_FLAG' => 'boolean'
	];

	protected $fillable = [
		'TYPE_WEAPON_ID',
		'DESCRIPTION',
		'PHOTO_ID',
		'STANDART_WEAPON_FLAG'
	];

	public function photo()
	{
		return $this->belongsTo(\App\Models\Photo::class, 'PHOTO_ID', 'PHOTO_ID');
	}

	public function type_weapon()
	{
		return $this->belongsTo(\App\Models\TypeWeapon::class, 'TYPE_WEAPON_ID', 'TYPE_WEAPON_ID');
	}

	public function orders()
	{
		return $this->hasMany(\App\Models\Order::class, 'WEAPON_ID', 'WEAPON_ID');
	}
}
