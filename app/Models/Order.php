<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 14 Nov 2017 21:18:16 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Order
 * 
 * @property int $order_id
 * @property int $TYPE_STATUS_ID
 * @property int $OBJECT_ID
 * @property int $WEAPON_ID
 * @property int $DECORATION_ID
 * @property int $AMOUNT
 * @property \Carbon\Carbon $DATE_ORDER
 * @property \Carbon\Carbon $DATE_EXECUTION
 * 
 * @property \App\Models\Decoration $decoration
 * @property \App\Models\Object $object
 * @property \App\Models\TypeStatus $type_status
 * @property \App\Models\Weapon $weapon
 * @property \Illuminate\Database\Eloquent\Collection $clients
 *
 * @package App\Models
 */
class Order extends Eloquent
{
	protected $primaryKey = 'order_id';
	public $timestamps = false;

	protected $casts = [
		'TYPE_STATUS_ID' => 'int',
		'OBJECT_ID' => 'int',
		'WEAPON_ID' => 'int',
		'DECORATION_ID' => 'int',
		'AMOUNT' => 'int'
	];

	protected $dates = [
		'DATE_ORDER',
		'DATE_EXECUTION'
	];

	protected $fillable = [
		'TYPE_STATUS_ID',
		'OBJECT_ID',
		'WEAPON_ID',
		'DECORATION_ID',
		'AMOUNT',
		'DATE_ORDER',
		'DATE_EXECUTION'
	];

	public function decoration()
	{
		return $this->belongsTo(\App\Models\Decoration::class, 'DECORATION_ID', 'DECORATION_ID');
	}

	public function object()
	{
		return $this->belongsTo(\App\Models\Object::class, 'OBJECT_ID', 'OBJECT_ID');
	}

	public function type_status()
	{
		return $this->belongsTo(\App\Models\TypeStatus::class, 'TYPE_STATUS_ID');
	}

	public function weapon()
	{
		return $this->belongsTo(\App\Models\Weapon::class, 'WEAPON_ID', 'WEAPON_ID');
	}

	public function clients()
	{
		return $this->belongsToMany(\App\Models\Client::class, 'client_orders', 'ORDER_ID', 'CLIENT_ID')
					->withPivot('client_order_id');
	}
}
