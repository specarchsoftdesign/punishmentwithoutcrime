<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 14 Nov 2017 21:18:16 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Object
 * 
 * @property int $object_id
 * @property int $TYPE_OBJECT_ID
 * @property int $INANIMATE_ID
 * @property int $ANIMAL_ID
 * @property int $HUMAN_ID
 * @property int $STANDART_OBJ_FLAG
 * 
 * @property \App\Models\Animal $animal
 * @property \App\Models\Human $human
 * @property \App\Models\Inanimate $inanimate
 * @property \App\Models\TypeObject $type_object
 * @property \Illuminate\Database\Eloquent\Collection $orders
 *
 * @package App\Models
 */
class Object extends Eloquent
{
	protected $table = 'object';
	protected $primaryKey = 'object_id';
	public $timestamps = false;

	protected $casts = [
		'TYPE_OBJECT_ID' => 'int',
		'INANIMATE_ID' => 'int',
		'ANIMAL_ID' => 'int',
		'HUMAN_ID' => 'int',
		'STANDART_OBJ_FLAG' => 'boolean'
	];

	protected $fillable = [
		'TYPE_OBJECT_ID',
		'INANIMATE_ID',
		'ANIMAL_ID',
		'HUMAN_ID',
		'STANDART_OBJ_FLAG'
	];

	public function animal()
	{
		return $this->belongsTo(\App\Models\Animal::class, 'ANIMAL_ID', 'ANIMAL_ID');
	}

	public function human()
	{
		return $this->belongsTo(\App\Models\Human::class, 'HUMAN_ID', 'HUMAN_ID');
	}

	public function inanimate()
	{
		return $this->belongsTo(\App\Models\Inanimate::class, 'INANIMATE_ID', 'INANIMATE_ID');
	}

	public function type_object()
	{
		return $this->belongsTo(\App\Models\TypeObject::class, 'TYPE_OBJECT_ID', 'TYPE_OBJECT_ID');
	}

	public function orders()
	{
		return $this->hasMany(\App\Models\Order::class, 'OBJECT_ID', 'OBJECT_ID');
	}
}
