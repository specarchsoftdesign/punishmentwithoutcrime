<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 14 Nov 2017 21:18:16 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class TypeDecoration
 * 
 * @property int $type_decoration_id
 * @property string $NAME
 * @property int $AMOUNT
 * @property int $SURCHARGE
 * 
 * @property \Illuminate\Database\Eloquent\Collection $decorations
 *
 * @package App\Models
 */
class TypeDecoration extends Eloquent
{
	protected $table = 'type_decoration';
	protected $primaryKey = 'type_decoration_id';
	public $timestamps = false;

	protected $casts = [
		'AMOUNT' => 'int',
		'SURCHARGE' => 'int'
	];

	protected $fillable = [
		'NAME',
		'AMOUNT',
		'SURCHARGE'
	];

	public function decorations()
	{
		return $this->hasMany(\App\Models\Decoration::class, 'TYPE_DECORATION_ID', 'TYPE_DECORATION_ID');
	}
}
