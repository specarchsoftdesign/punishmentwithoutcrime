<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 14 Nov 2017 21:18:16 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ClientOrder
 * 
 * @property int $client_order_id
 * @property int $CLIENT_ID
 * @property int $ORDER_ID
 * 
 * @property \App\Models\Client $client
 * @property \App\Models\Order $order
 *
 * @package App\Models
 */
class ClientOrder extends Eloquent
{
	protected $primaryKey = 'client_order_id';
	public $timestamps = false;

	protected $casts = [
		'CLIENT_ID' => 'int',
		'ORDER_ID' => 'int'
	];

	protected $fillable = [
		'CLIENT_ID',
		'ORDER_ID'
	];

	public function client()
	{
		return $this->belongsTo(\App\Models\Client::class, 'CLIENT_ID');
	}

	public function order()
	{
		return $this->belongsTo(\App\Models\Order::class, 'ORDER_ID', 'ORDER_ID');
	}
}
