<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 14 Nov 2017 21:18:16 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class TypeStatus
 * 
 * @property int $TYPE_STATUS_ID
 * @property string $NAME
 * 
 * @property \Illuminate\Database\Eloquent\Collection $orders
 *
 * @package App\Models
 */
class TypeStatus extends Eloquent
{
	protected $table = 'type_status';
	protected $primaryKey = 'TYPE_STATUS_ID';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'TYPE_STATUS_ID' => 'int'
	];

	protected $fillable = [
		'NAME'
	];

	public function orders()
	{
		return $this->hasMany(\App\Models\Order::class, 'TYPE_STATUS_ID');
	}
}
