<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 14 Nov 2017 21:18:16 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Decoration
 * 
 * @property int $decoration_id
 * @property int $TYPE_DECORATION_ID
 * @property string $DESCRIPTION
 * @property int $PHOTO_ID
 * @property int $STANDART_DECOR_FLAG
 * 
 * @property \App\Models\Photo $photo
 * @property \App\Models\TypeDecoration $type_decoration
 * @property \Illuminate\Database\Eloquent\Collection $orders
 *
 * @package App\Models
 */
class Decoration extends Eloquent
{
	protected $table = 'decoration';
	protected $primaryKey = 'decoration_id';
	public $timestamps = false;

	protected $casts = [
		'TYPE_DECORATION_ID' => 'int',
		'PHOTO_ID' => 'int',
		'STANDART_DECOR_FLAG' =>'boolean'
	];

	protected $fillable = [
		'TYPE_DECORATION_ID',
		'DESCRIPTION',
		'PHOTO_ID',
		'STANDART_DECOR_FLAG'
	];

	public function photo()
	{
		return $this->belongsTo(\App\Models\Photo::class, 'PHOTO_ID', 'PHOTO_ID');
	}

	public function type_decoration()
	{
		return $this->belongsTo(\App\Models\TypeDecoration::class, 'TYPE_DECORATION_ID', 'TYPE_DECORATION_ID');
	}

	public function orders()
	{
		return $this->hasMany(\App\Models\Order::class, 'DECORATION_ID', 'DECORATION_ID');
	}
}
