<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 14 Nov 2017 21:18:16 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class TypeAccount
 * 
 * @property int $TYPE_ACCOUNT_ID
 * @property string $NAME
 * 
 * @property \Illuminate\Database\Eloquent\Collection $accounts
 *
 * @package App\Models
 */
class TypeAccount extends Eloquent
{
	protected $table = 'type_account';
	protected $primaryKey = 'TYPE_ACCOUNT_ID';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'TYPE_ACCOUNT_ID' => 'int'
	];

	protected $fillable = [
		'NAME'
	];

	public function accounts()
	{
		return $this->hasMany(\App\Models\Account::class, 'TYPE_ACCOUNT_ID');
	}
}
