<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 14 Nov 2017 21:18:16 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Human
 * 
 * @property int $human_id
 * @property string $SECOND_NAME
 * @property string $FIRST_NAME
 * @property string $PATHRONYMIC
 * @property string $GENDER
 * @property int $AGE
 * @property int $GROWTH
 * @property int $WEIGHT
 * @property string $EYE_COLOR
 * @property string $HAIR_COLOR
 * @property string $DESCRIPTION
 * @property int $PHOTO_ID
 * 
 * @property \App\Models\Photo $photo
 * @property \Illuminate\Database\Eloquent\Collection $objects
 *
 * @package App\Models
 */
class Human extends Eloquent
{
	protected $table = 'human';
	protected $primaryKey = 'human_id';
	public $timestamps = false;

	protected $casts = [
		'AGE' => 'int',
		'GROWTH' => 'int',
		'WEIGHT' => 'int',
		'PHOTO_ID' => 'int'
	];

	protected $fillable = [
		'SECOND_NAME',
		'FIRST_NAME',
		'PATHRONYMIC',
		'GENDER',
		'AGE',
		'GROWTH',
		'WEIGHT',
		'EYE_COLOR',
		'HAIR_COLOR',
		'DESCRIPTION',
		'PHOTO_ID'
	];

	public function photo()
	{
		return $this->belongsTo(\App\Models\Photo::class, 'PHOTO_ID', 'PHOTO_ID');
	}

	public function objects()
	{
		return $this->hasMany(\App\Models\Object::class, 'HUMAN_ID', 'HUMAN_ID');
	}
}
