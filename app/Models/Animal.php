<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 14 Nov 2017 21:18:15 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Animal
 * 
 * @property int $animal_id
 * @property string $KIND
 * @property string $DESCRIPTION
 * @property int $PHOTO_ID
 * 
 * @property \App\Models\Photo $photo
 * @property \Illuminate\Database\Eloquent\Collection $objects
 *
 * @package App\Models
 */
class Animal extends Eloquent
{
	protected $table = 'animal';
	protected $primaryKey = 'animal_id';
	public $timestamps = false;

	protected $casts = [
		'PHOTO_ID' => 'int'
	];

	protected $fillable = [
		'KIND',
		'DESCRIPTION',
		'PHOTO_ID'
	];

	public function photo()
	{
		return $this->belongsTo(\App\Models\Photo::class, 'PHOTO_ID', 'PHOTO_ID');
	}

	public function objects()
	{
		return $this->hasMany(\App\Models\Object::class, 'ANIMAL_ID', 'ANIMAL_ID');
	}
}
