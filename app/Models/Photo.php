<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 14 Nov 2017 21:18:16 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Photo
 * 
 * @property int $photo_id
 * @property string $PHOTO_1
 * @property string $PHOTO_2
 * @property string $PHOTO_3
 * 
 * @property \Illuminate\Database\Eloquent\Collection $animals
 * @property \Illuminate\Database\Eloquent\Collection $decorations
 * @property \Illuminate\Database\Eloquent\Collection $humans
 * @property \Illuminate\Database\Eloquent\Collection $inanimates
 * @property \Illuminate\Database\Eloquent\Collection $weapons
 *
 * @package App\Models
 */
class Photo extends Eloquent
{
	protected $table = 'photo';
	protected $primaryKey = 'photo_id';
	public $timestamps = false;

	protected $fillable = [
		'PHOTO_1',
		'PHOTO_2',
		'PHOTO_3'
	];

	public function animals()
	{
		return $this->hasMany(\App\Models\Animal::class, 'PHOTO_ID', 'PHOTO_ID');
	}

	public function decorations()
	{
		return $this->hasMany(\App\Models\Decoration::class, 'PHOTO_ID', 'PHOTO_ID');
	}

	public function humans()
	{
		return $this->hasMany(\App\Models\Human::class, 'PHOTO_ID', 'PHOTO_ID');
	}

	public function inanimates()
	{
		return $this->hasMany(\App\Models\Inanimate::class, 'PHOTO_ID', 'PHOTO_ID');
	}

	public function weapons()
	{
		return $this->hasMany(\App\Models\Weapon::class, 'PHOTO_ID', 'PHOTO_ID');
	}
}
