<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 25 Nov 2017 20:54:27 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class CompanySetting
 * 
 * @property int $COUNT_ORDER_LEVEL_2
 * @property int $COUNT_ORDER_LEVEL_3
 *
 * @package App\Models
 */
class CompanySetting extends Eloquent
{
	protected $table = 'company_setting';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'COUNT_ORDER_LEVEL_2' => 'int',
		'COUNT_ORDER_LEVEL_3' => 'int'
	];

	protected $fillable = [
		'COUNT_ORDER_LEVEL_2',
		'COUNT_ORDER_LEVEL_3'
	];
}
