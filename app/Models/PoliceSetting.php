<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 14 Nov 2017 21:18:16 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PoliceSetting
 * 
 * @property int $THRESHOLD_CRIME
 * @property int $THRESHOLD_LEVEL_1
 * @property int $THRESHOLD_LEVEL_2
 * @property int $THRESHOLD_LEVEL_3
 *
 * @package App\Models
 */
class PoliceSetting extends Eloquent
{
	protected $table = 'police_setting';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'THRESHOLD_CRIME' => 'int',
		'THRESHOLD_LEVEL_1' => 'int',
		'THRESHOLD_LEVEL_2' => 'int',
		'THRESHOLD_LEVEL_3' => 'int'
	];

	protected $fillable = [
		'THRESHOLD_CRIME',
		'THRESHOLD_LEVEL_1',
		'THRESHOLD_LEVEL_2',
		'THRESHOLD_LEVEL_3'
	];
}
