<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 14 Nov 2017 21:18:16 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Inanimate
 * 
 * @property int $inanimate_id
 * @property string $NAME
 * @property string $DESCRIPTION
 * @property int $PHOTO_ID
 * 
 * @property \App\Models\Photo $photo
 * @property \Illuminate\Database\Eloquent\Collection $objects
 *
 * @package App\Models
 */
class Inanimate extends Eloquent
{
	protected $table = 'inanimate';
	protected $primaryKey = 'inanimate_id';
	public $timestamps = false;

	protected $casts = [
		'PHOTO_ID' => 'int'
	];

	protected $fillable = [
		'NAME',
		'DESCRIPTION',
		'PHOTO_ID'
	];

	public function photo()
	{
		return $this->belongsTo(\App\Models\Photo::class, 'PHOTO_ID', 'PHOTO_ID');
	}

	public function objects()
	{
		return $this->hasMany(\App\Models\Object::class, 'INANIMATE_ID', 'INANIMATE_ID');
	}
}
