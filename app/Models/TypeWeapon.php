<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 14 Nov 2017 21:18:16 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class TypeWeapon
 * 
 * @property int $type_weapon_id
 * @property string $NAME
 * @property int $AMOUNT
 * @property int $SURCHARGE
 * 
 * @property \Illuminate\Database\Eloquent\Collection $weapons
 *
 * @package App\Models
 */
class TypeWeapon extends Eloquent
{
	protected $table = 'type_weapon';
	protected $primaryKey = 'type_weapon_id';
	public $timestamps = false;

	protected $casts = [
		'AMOUNT' => 'int',
		'SURCHARGE' => 'int'
	];

	protected $fillable = [
		'NAME',
		'AMOUNT',
		'SURCHARGE'
	];

	public function weapons()
	{
		return $this->hasMany(\App\Models\Weapon::class, 'TYPE_WEAPON_ID', 'TYPE_WEAPON_ID');
	}
}
