<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Request;
use Auth;
use Illuminate\Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
     protected function redirectTo()
     {
        $input = Request::all();
        if(Auth::attempt(['login' => $input['login'],'password' => $input['password']])){
            if (Auth::check()){
                session(['user' => Auth::user()]);
                //return dd(\Auth::user());
                // в зависиммости от типа аккаунта перенаправляем на разные страницы
                return '/company_profile';
            }
        }
        else{
            return 'vse ploho';
        }
            
     }

     public function username()
     {
         return 'login';
     }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
