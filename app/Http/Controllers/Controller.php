<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
	
	function home()
	{
		// тут тоже надо вставить проверку, что пользователь ещё не авторизован
		return view('home');
	}
	
	function registration()
	{
		// тут тоже надо вставить проверку, что пользователь ещё не авторизован
		return view('registration');
	}

	function main()
	{
		return view('about_services_client');
	}

	function main_firm()
	{
		return view('about_services_firm');
	}

	function main_police()
	{
		return view('about_services_pravoohran');
	}
}
