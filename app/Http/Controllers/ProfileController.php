<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Request;
use Illuminate\Support\Facades\DB;

class ProfileController extends Controller
{
    function showProfile()
	{
        if(session()->has('user')){
            if(session('user')['TYPE_ACCOUNT_ID']==1 || Auth::user()['TYPE_ACCOUNT_ID']==4){
                $data = $this->getProfileInfo(session('user')['account_id']);
                return view('client_profile', $data);
            }
            else if(session('user')['TYPE_ACCOUNT_ID']==2){
                $orders = DB::select('select o.ORDER_ID, o.DATE_ORDER, concat_ws(\' \',ac.last_name,ac.first_name,ac.pathronymic) as fio, ac.login, t_s.NAME as status_name, t_obj.type_object_name, o.AMOUNT from client_orders as c_o
                natural join (select * from client as c natural join account) as ac
                natural join orders as o
                natural join (select object_id,name as type_object_name from object natural join type_object) as t_obj
                natural join type_status as t_s');
                return view('firm_profile',['orders'=>$orders]);
            }
            else if(session('user')['TYPE_ACCOUNT_ID']==3){
                $orders = DB::select('select o.ORDER_ID, o.DATE_ORDER, concat_ws(\' \',ac.last_name,ac.first_name,ac.pathronymic) as fio, ac.login, t_s.NAME as status_name, t_obj.type_object_name, o.AMOUNT from client_orders as c_o
                natural join (select * from client as c natural join account) as ac
                natural join orders as o
                natural join (select object_id,name as type_object_name from object natural join type_object) as t_obj
                natural join type_status as t_s');
                return view('pravoohran_profile',['orders'=>$orders]);
            }
            else
                abort(404);
        }
        else{
            return view('/');
        }
    }

    function getProfileInfo($id){
        $data = [];
        $last_name = DB::select('select LAST_NAME from Client where ACCOUNT_ID = ?',[$id]);
        $first_name = DB::select('select FIRST_NAME from Client where ACCOUNT_ID = ?',[$id]);
        $pathronymic = DB::select('select PATHRONYMIC from Client where ACCOUNT_ID = ?',[$id]);
        $type_account_id = DB::select('select TYPE_ACCOUNT_ID from account where ACCOUNT_ID = ?',[$id]);
        if($type_account_id[0]->TYPE_ACCOUNT_ID == 4)
            $data['status'] = 'заблокирован';
        else
            $data['status'] = 'активен';
        
        $data['name'] = $last_name[0]->LAST_NAME . ' ' . $first_name[0]->FIRST_NAME . ' ' . $pathronymic[0]->PATHRONYMIC;
        $data['phone'] =  DB::select('select PHONE_NUMBER from Client where ACCOUNT_ID = ?',[$id])[0]->PHONE_NUMBER;
        $data['date'] = DB::select('select BIRTH_DATE from Client where ACCOUNT_ID = ?',[$id])[0]->BIRTH_DATE;
        return $data;
    }

    function showClientHistory(){
        $id = session('user')['account_id'];
        $orders = DB::select('select * from client_orders c_o
        natural join orders 
        natural join client c
        natural join (select object_id,name as type_object_name from object natural join type_object) as t_obj
        natural join type_status
        where ACCOUNT_ID = ?',[$id]);
        return view('history_client',['orders'=>$orders]);
    }

    //одобрить заказ
    function processOrders(){
        $input = Request::all();
        if(isset($input['orders_id'])){
            $orders = $input['orders_id'];
            foreach($orders as $ord){
                $need_approve = DB::select('select ORDER_ID,TYPE_STATUS_ID from orders where ORDER_ID=?',[$ord]);
                if(isset($input['btn-approve'])) {
                    if($need_approve[0]->TYPE_STATUS_ID == 1){
                        $affected = DB::update('update orders set type_status_id=? where ORDER_ID=?',[2,$ord]);
                    }
                }
                else if(isset($input['btn-cancel'])){
                    if($need_approve[0]->TYPE_STATUS_ID == 1){
                        $affected = DB::update('update orders set type_status_id=? where ORDER_ID=?',[4,$ord]);
                    }
                }
                else if(isset($input['btn-confirm'])){
                    if($need_approve[0]->TYPE_STATUS_ID == 2){
                        $affected = DB::update('update orders set type_status_id=? where ORDER_ID=?',[3,$ord]);
                    }
                }
            }
            DB::commit();
        }
        if(isset($input['btn-apply-filter'])){
            $req = 'select o.ORDER_ID, o.DATE_ORDER, concat_ws(\' \',ac.last_name,ac.first_name,ac.pathronymic) as fio, 
                                ac.login, t_s.NAME as status_name, t_obj.type_object_name, o.AMOUNT,
                                case when ac.type_account_id != 4 then \'активен\' else \'закрыт\' end as account_status,
                                t_obj.type_object_id,t_s.type_status_id,ac.type_account_id
                                from client_orders as c_o
                    natural join (select * from client as c natural join account) as ac
                    natural join orders as o
                    natural join (select object_id,name as type_object_name, TYPE_OBJECT_ID from object natural join type_object) as t_obj
                    natural join type_status as t_s';
            $filter = '';
            if($input['input-filter-login'] != null){
                $filter .= ' login like \'' . $input['input-filter-login'] . '%\'';
            }
            if($input['input_filter_fio'] != null){
                if(strlen($filter) != 0){
                    $filter .= ' and ';
                }
                $filter .= 'concat_ws(\' \',ac.last_name,ac.first_name,ac.pathronymic) like \'' . $input['input_filter_fio'] . '%\''; 
            }
            if($input['input-filter-typeobj'] != '--'){
                if(strlen($filter) != 0){
                    $filter .= ' and ';
                }
                $filter .= 'type_object_id = ' . $input['input-filter-typeobj']; 
            }
            if($input['input-filter-price-min'] != null && $input['input-filter-price-max'] != null){
                if(strlen($filter) != 0){
                    $filter .= ' and ';
                }
                $filter .= 'amount between ' . $input['input-filter-price-min'] . ' and ' . $input['input-filter-price-max']; 
            }
            if($input['input-filter-price-min'] != null && $input['input-filter-price-max'] == null){
                if(strlen($filter) != 0){
                    $filter .= ' and ';
                }
                $filter .= 'amount >= ' . $input['input-filter-price-min']; 
            }
            if($input['input-filter-price-min'] == null && $input['input-filter-price-max'] != null){
                if(strlen($filter) != 0){
                    $filter .= ' and ';
                }
                $filter .= 'amount <= ' . $input['input-filter-price-max']; 
            }
            if($input['input-filter-date-min'] != null && $input['input-filter-date-max'] != null){
                if(strlen($filter) != 0){
                    $filter .= ' and ';
                }
                $filter .= 'DATE_ORDER between ' . $input['input-filter-date-min'] . ' and ' . $input['input-date-price-max']; 
            }
            if($input['input-filter-date-min'] != null && $input['input-filter-date-max'] == null){
                if(strlen($filter) != 0){
                    $filter .= ' and ';
                }
                $filter .= 'DATE_ORDER >= ' . $input['input-filter-date-min']; 
            }
            if($input['input-filter-date-min'] == null && $input['input-filter-date-max'] != null){
                if(strlen($filter) != 0){
                    $filter .= ' and ';
                }
                $filter .= 'DATE_ORDER <= ' . $input['input-filter-date-max']; 
            }
            if($input['input-filter-order-status'] != '--' ){
                if(strlen($filter) != 0){
                    $filter .= ' and ';
                }
                $filter .= 'type_status_id = ' . $input['input-filter-order-status']; 
            }
            if($input['input-filter-acc-status'] != '--' ){
                if(strlen($filter) != 0){
                    $filter .= ' and ';
                }
                $filter .= 'type_account_id = ' . $input['input-filter-acc-status']; 
            }

            if(strlen($filter)!=0){
                $orders = DB::select($req . ' where ' . $filter);
            }
            else{
                $orders = DB::select($req);
            }
            return view('firm_profile',['orders'=>$orders]);
        }
        if(isset($input['btn-reset-filter'])){
            $req = 'select o.ORDER_ID, o.DATE_ORDER, concat_ws(\' \',ac.last_name,ac.first_name,ac.pathronymic) as fio, 
                                ac.login, t_s.NAME as status_name, t_obj.type_object_name, o.AMOUNT,
                                case when ac.type_account_id != 4 then \'активен\' else \'закрыт\' end as account_status
                                from client_orders as c_o
                    natural join (select * from client as c natural join account) as ac
                    natural join orders as o
                    natural join (select object_id,name as type_object_name from object natural join type_object) as t_obj
                    natural join type_status as t_s';
            $orders = DB::select($req);
            return view('firm_profile',['orders'=>$orders]);
        }
        return redirect('history_orders_firm');
        
    }
    
    function showHistoryForFirm(){
        if(Auth::check()){
            if(Auth::user()['TYPE_ACCOUNT_ID']==2){
                $input = Request::all();
                
                    $orders = DB::select('select o.ORDER_ID, o.DATE_ORDER, concat_ws(\' \',ac.last_name,ac.first_name,ac.pathronymic) as fio, 
                                        ac.login, t_s.NAME as status_name, t_obj.type_object_name, o.AMOUNT,
                                        case when ac.type_account_id != 4 then \'активен\' else \'закрыт\' end as account_status from client_orders as c_o
                    natural join (select * from client as c natural join account) as ac
                    natural join orders as o
                    natural join (select object_id,name as type_object_name from object natural join type_object) as t_obj
                    natural join type_status as t_s');
                
                return view('firm_profile',['orders'=>$orders]);
            }
            else
                abort(404);
        }
        else{
            return view('/');
        }
    }

    function showHistoryForPolice(){
        if(session()->has('user')){
            if(session('user')['TYPE_ACCOUNT_ID']==3){
                $orders = DB::select('select o.ORDER_ID, o.DATE_ORDER, concat_ws(\' \',ac.last_name,ac.first_name,ac.pathronymic) as fio, 
                                        ac.login, t_s.NAME as status_name, t_obj.type_object_name, o.AMOUNT,
                                        case when ac.type_account_id != 4 then \'активен\' else \'закрыт\' end as account_status from client_orders as c_o
                        natural join (select * from client as c natural join account) as ac
                        natural join orders as o
                        natural join (select object_id,name as type_object_name from object natural join type_object) as t_obj
                        natural join type_status as t_s');
                return view('pravoohran_profile',['orders'=>$orders]);
            }
            else
                abort(404);
        }
        else{
            return view('/');
        }
    }

    function addOrder(){
        if(session()->has('user')){
            if(session('user')['TYPE_ACCOUNT_ID']==1 || Auth::user()['TYPE_ACCOUNT_ID']==4){
                
                $input = Request::all();

                return dd($input);
            }
            else
                abort(404);
        }
        else{
            return view('/');
        }
    }

    function filter($input){
        $req = 'select o.ORDER_ID, o.DATE_ORDER, concat_ws(\' \',ac.last_name,ac.first_name,ac.pathronymic) as fio, 
                ac.login, t_s.NAME as status_name, t_obj.type_object_name, o.AMOUNT,
                case when ac.type_account_id != 4 then \'активен\' else \'закрыт\' end as account_status,
                t_obj.type_object_id,t_s.type_status_id,ac.type_account_id
                from client_orders as c_o
        natural join (select * from client as c natural join account) as ac
        natural join orders as o
        natural join (select object_id,name as type_object_name, TYPE_OBJECT_ID from object natural join type_object) as t_obj
        natural join type_status as t_s';
        $filter = '';
        if($input['input-filter-login'] != null){
            $filter .= ' login like \'' . $input['input-filter-login'] . '%\'';
        }
        if($input['input_filter_fio'] != null){
            if(strlen($filter) != 0){
                $filter .= ' and ';
            }
            $filter .= 'concat_ws(\' \',ac.last_name,ac.first_name,ac.pathronymic) like \'' . $input['input_filter_fio'] . '%\''; 
        }
        if($input['input-filter-typeobj'] != '--'){
            if(strlen($filter) != 0){
                $filter .= ' and ';
            }
            $filter .= 'type_object_id = ' . $input['input-filter-typeobj']; 
        }
        if($input['input-filter-price-min'] != null && $input['input-filter-price-max'] != null){
            if(strlen($filter) != 0){
                $filter .= ' and ';
            }
            $filter .= 'amount between ' . $input['input-filter-price-min'] . ' and ' . $input['input-filter-price-max']; 
        }
        if($input['input-filter-price-min'] != null && $input['input-filter-price-max'] == null){
            if(strlen($filter) != 0){
                $filter .= ' and ';
            }
            $filter .= 'amount >= ' . $input['input-filter-price-min']; 
        }
        if($input['input-filter-price-min'] == null && $input['input-filter-price-max'] != null){
            if(strlen($filter) != 0){
                $filter .= ' and ';
            }
            $filter .= 'amount <= ' . $input['input-filter-price-max']; 
        }
        if($input['input-filter-date-min'] != null && $input['input-filter-date-max'] != null){
            if(strlen($filter) != 0){
                $filter .= ' and ';
            }
            $filter .= 'DATE_ORDER between ' . $input['input-filter-date-min'] . ' and ' . $input['input-date-price-max']; 
        }
        if($input['input-filter-date-min'] != null && $input['input-filter-date-max'] == null){
            if(strlen($filter) != 0){
                $filter .= ' and ';
            }
            $filter .= 'DATE_ORDER >= ' . $input['input-filter-date-min']; 
        }
        if($input['input-filter-date-min'] == null && $input['input-filter-date-max'] != null){
            if(strlen($filter) != 0){
                $filter .= ' and ';
            }
            $filter .= 'DATE_ORDER <= ' . $input['input-filter-date-max']; 
        }
        if($input['input-filter-order-status'] != '--' ){
            if(strlen($filter) != 0){
                $filter .= ' and ';
            }
            $filter .= 'type_status_id = ' . $input['input-filter-order-status']; 
        }
        if($input['input-filter-acc-status'] != '--' ){
            if(strlen($filter) != 0){
                $filter .= ' and ';
            }
            $filter .= 'type_account_id = ' . $input['input-filter-acc-status']; 
        }

        if(strlen($filter)!=0){
            $orders = DB::select($req . ' where ' . $filter);
        }
        else{
            $orders = DB::select($req);
        }
        return $order;
    }

    // задержать
    function toImprison(){
        $input = Request::all();
        if(isset($input['orders_id'])){
            $orders = $input['orders_id'];
            foreach($orders as $ord){
                $need_imprison = DB::select('select CLIENT_ID from client_orders where ORDER_ID=?',[$ord]);               
                $account_id = DB::select('select ACCOUNT_ID from client where client_id=?',[$need_imprison[0]->CLIENT_ID]);                  
                $affected = DB::update('update account set TYPE_ACCOUNT_ID=? where ACCOUNT_ID=?',[4,$account_id[0]->ACCOUNT_ID]);
            }
            DB::commit();
        }
    }

    // амнистировать
    function toAmnesty(){
        $input = Request::all();
        if(isset($input['orders_id'])){
            $orders = $input['orders_id'];
            foreach($orders as $ord){
                $need_imprison = DB::select('select CLIENT_ID from client_orders where ORDER_ID=?',[$ord]);               
                $account_id = DB::select('select ACCOUNT_ID from client where client_id=?',[$need_imprison[0]->CLIENT_ID]);                  
                $affected = DB::update('update account set TYPE_ACCOUNT_ID=? where ACCOUNT_ID=?',[1,$account_id[0]->ACCOUNT_ID]);
            }
            DB::commit();
        }
    }

    function changeAccountStatus(){
        $input = Request::all();
        if(isset($input['btn-organ'])) { 
            $this->toImprison(); 
        }
        else if(isset($input['btn-butpolice'])) { 
            $this->toAmnesty(); 
        }
        //dd($input);
        if(isset($input['btn-apply-filter'])){
            $req = 'select o.ORDER_ID, o.DATE_ORDER, concat_ws(\' \',ac.last_name,ac.first_name,ac.pathronymic) as fio, 
                                ac.login, t_s.NAME as status_name, t_obj.type_object_name, o.AMOUNT,
                                case when ac.type_account_id != 4 then \'активен\' else \'закрыт\' end as account_status,
                                t_obj.type_object_id,t_s.type_status_id,ac.type_account_id
                                from client_orders as c_o
                    natural join (select * from client as c natural join account) as ac
                    natural join orders as o
                    natural join (select object_id,name as type_object_name, TYPE_OBJECT_ID from object natural join type_object) as t_obj
                    natural join type_status as t_s';
            $filter = '';
            if($input['input-filter-login'] != null){
                $filter .= ' login like \'' . $input['input-filter-login'] . '%\'';
            }
            if($input['input_filter_fio'] != null){
                if(strlen($filter) != 0){
                    $filter .= ' and ';
                }
                $filter .= 'concat_ws(\' \',ac.last_name,ac.first_name,ac.pathronymic) like \'' . $input['input_filter_fio'] . '%\''; 
            }
            if($input['input-filter-typeobj'] != '--'){
                if(strlen($filter) != 0){
                    $filter .= ' and ';
                }
                $filter .= 'type_object_id = ' . $input['input-filter-typeobj']; 
            }
            if($input['input-filter-price-min'] != null && $input['input-filter-price-max'] != null){
                if(strlen($filter) != 0){
                    $filter .= ' and ';
                }
                $filter .= 'amount between ' . $input['input-filter-price-min'] . ' and ' . $input['input-filter-price-max']; 
            }
            if($input['input-filter-price-min'] != null && $input['input-filter-price-max'] == null){
                if(strlen($filter) != 0){
                    $filter .= ' and ';
                }
                $filter .= 'amount >= ' . $input['input-filter-price-min']; 
            }
            if($input['input-filter-price-min'] == null && $input['input-filter-price-max'] != null){
                if(strlen($filter) != 0){
                    $filter .= ' and ';
                }
                $filter .= 'amount <= ' . $input['input-filter-price-max']; 
            }
            if($input['input-filter-date-min'] != null && $input['input-filter-date-max'] != null){
                if(strlen($filter) != 0){
                    $filter .= ' and ';
                }
                $filter .= 'DATE_ORDER between ' . $input['input-filter-date-min'] . ' and ' . $input['input-date-price-max']; 
            }
            if($input['input-filter-date-min'] != null && $input['input-filter-date-max'] == null){
                if(strlen($filter) != 0){
                    $filter .= ' and ';
                }
                $filter .= 'DATE_ORDER >= ' . $input['input-filter-date-min']; 
            }
            if($input['input-filter-date-min'] == null && $input['input-filter-date-max'] != null){
                if(strlen($filter) != 0){
                    $filter .= ' and ';
                }
                $filter .= 'DATE_ORDER <= ' . $input['input-filter-date-max']; 
            }
            if($input['input-filter-order-status'] != '--' ){
                if(strlen($filter) != 0){
                    $filter .= ' and ';
                }
                $filter .= 'type_status_id = ' . $input['input-filter-order-status']; 
            }
            if($input['input-filter-acc-status'] != '--' ){
                if(strlen($filter) != 0){
                    $filter .= ' and ';
                }
                $filter .= 'type_account_id = ' . $input['input-filter-acc-status']; 
            }

            if(strlen($filter)!=0){
                $orders = DB::select($req . ' where ' . $filter);
            }
            else{
                $orders = DB::select($req);
            }
            return view('pravoohran_profile',['orders'=>$orders]);
        }
        if(isset($input['btn-reset-filter'])){
            $req = 'select o.ORDER_ID, o.DATE_ORDER, concat_ws(\' \',ac.last_name,ac.first_name,ac.pathronymic) as fio, 
                                ac.login, t_s.NAME as status_name, t_obj.type_object_name, o.AMOUNT,
                                case when ac.type_account_id != 4 then \'активен\' else \'закрыт\' end as account_status
                                from client_orders as c_o
                    natural join (select * from client as c natural join account) as ac
                    natural join orders as o
                    natural join (select object_id,name as type_object_name from object natural join type_object) as t_obj
                    natural join type_status as t_s';
            $orders = DB::select($req);
            return view('pravoohran_profile',['orders'=>$orders]);
        }
        return redirect('history_orders_police');
    }
}
