<?php

namespace App\Http\Controllers;

use Request;
use Illuminate\Support\Facades\DB;
use App\Events\ObjectCategoryChanged;
use App\Events\WeaponCategoryChanged;

class OrderController extends Controller
{
    function makeOrder(){
        if(session()->has('user')){
            if(session('user')['TYPE_ACCOUNT_ID']==1 || Auth::user()['TYPE_ACCOUNT_ID']==4){
                $data = [];
                $data['types_object'] = DB::select('select NAME from TYPE_OBJECT');
                $data['types_weapon'] = DB::select('select NAME from TYPE_WEAPON');
                $data['types_decoration'] = DB::select('select NAME from TYPE_DECORATION');
                return view('order_of_services', $data);
            }
            else
                abort(404);
        }
        else
            return redirect('/');
    }

    function objectCategoryChanged(){
        $input = Request::all();
        broadcast(new ObjectCategoryChanged($input));
        return;
    }

    function weaponCategoryChanged(){
        $input = Request::all();
        broadcast(new WeaponCategoryChanged($input));
        return;
    }

    function checkout(){
        if(session()->has('user')){
            if(session('user')['TYPE_ACCOUNT_ID']==1 || Auth::user()['TYPE_ACCOUNT_ID']==4){
                $input = Request::all();
                //return dd($input);
                
                // Если нажата кнопка формирования чека
                if(isset($input['order-serv'])) {                  
                    $input = Request::all();

                    // Нельзя оставлять это поле пустым, оно участвует в рассчетах
                    if(empty($input['object_price'])){

                        $input['object_price'] = 200000;
                    }

                    $input['id'] =  DB::select('select max(order_id) as maximum from orders')[0]->maximum+1;
                    // Берем данные из таблицы, либо из полей и БД (всё что ниже надо будет дописать)
                    if($input['object-radio-list'] == 'table-object'){// Если выбран объект из таблицы
                        $id = $input['object-radio-in-table'];
                        if($input['object_category'] == 'человек'){
                            $input['object_description'] = DB::select('select DESCRIPTION from HUMAN where human_id=?',[$id])[0]->DESCRIPTION;
                            $input['first_name'] = DB::select('select FIRST_NAME from HUMAN where human_id=?',[$id])[0]->FIRST_NAME;
                            $input['second_name'] = DB::select('select SECOND_NAME from HUMAN where human_id=?',[$id])[0]->SECOND_NAME;
                            $input['pathronymic'] = DB::select('select PATHRONYMIC from HUMAN where human_id=?',[$id])[0]->PATHRONYMIC;
                            $input['gender'] = DB::select('select GENDER from HUMAN where human_id=?',[$id])[0]->GENDER;
                            $input['age'] = DB::select('select AGE from HUMAN where human_id=?',[$id])[0]->AGE;
                            $input['growth'] = DB::select('select GROWTH from HUMAN where human_id=?',[$id])[0]->GROWTH;
                            $input['weight'] = DB::select('select WEIGHT from HUMAN where human_id=?',[$id])[0]->WEIGHT;
                            $input['eyes_color'] = DB::select('select EYE_COLOR from HUMAN where human_id=?',[$id])[0]->EYE_COLOR;
                            $input['hair_color'] = DB::select('select HAIR_COLOR from HUMAN where human_id=?',[$id])[0]->HAIR_COLOR;
                        }
                        else if($input['object_category'] == 'животное'){
                            $input['object_description'] = DB::select('select DESCRIPTION from ANIMAL where animal_id=?',[$id])[0]->DESCRIPTION;
                            $input['type_animal'] = DB::select('select KIND from ANIMAL where amimal_id=?',[$id])[0]->KIND;                          
                        }
                        else{
                            $input['object_description'] = DB::select('select DESCRIPTION from INANIMATE where inanimate_id=?',[$id])[0]->DESCRIPTION;
                            $input['name_inanimate'] = DB::select('select NAME from INANIMATE where inanimate_id=?',[$id])[0]->NAME;                          
                        }
                    }
                    if($input['object_category'] == 'человек')
                        $input['object_price'] = 100000;
                    else if($input['object_category'] == 'животное')
                        $input['object_price'] = 20000;
                    else
                        $input['object_price'] = 5000;

                    if($input['weapon-radio-list'] == 'table-weapon'){// Если выбрано оружие из таблицы
                        $id = $input['weapon-radio-in-table'];
                        $input['weapon_description'] = DB::select('select DESCRIPTION from WEAPON where weapon_id=?',[$id])[0]->DESCRIPTION;
                    }
                    
                    if($input['weapon_category'] == 'холодное'){
                        $input['weapon_price'] = 15000;
                    }
                    else if($input['weapon_category'] == 'огнестрельное'){
                        $input['weapon_price'] = 20000;
                    }
                    else{
                        $input['weapon_price'] = 10000;
                    }

                    if($input['decoration-radio-list'] == 'table-decoration'){// Если выбрана декорация из таблицы
                        $id = $input['decoration-radio-in-table'];
                        $input['decoration_description'] = DB::select('select DESCRIPTION from DECORATION where decoration_id=?',[$id])[0]->DESCRIPTION;
                    }

                    if($input['decoration_category'] == 'природа'){
                        $input['decoration_price'] = 15000;
                    }
                    else if($input['decoration_category'] == 'места общественного питания'){
                        $input['decoration_price'] = 20000;
                    }
                    else{ 
                        $input['decoration_price'] = 30000;
                    }

                    $input['common_price'] = $input['object_price'] + $input['weapon_price'] + $input['decoration_price'];

                    // Сохранение файлов в папку app\public\custom\<category_name>\<id>_<file_number>.<ext>
                    $input['objectPhotos'] = $this->uploadFiles(Request::file('files'), storage_path('app\public\custom\objects'), $input['id'], 3);
                    $input['gunPhotos'] = $this->uploadFiles(Request::file('files-gun'), storage_path('app\public\custom\guns'), $input['id'], 3);
                    $input['decorationPhotos'] = $this->uploadFiles(Request::file('files-loacle'), storage_path('app\public\custom\decorations'), $input['id'], 3);
                    
                    // Сохраняем в файл app\public\order_<id>.dat текстовые данные заказа
                    $buf = $input;
                    $buf['files'] = null;
                    $buf['files-gun'] = null;
                    $buf['files-locale'] = null;
                    $_input = serialize($buf);
                    file_put_contents(storage_path('app\public\order_'.$input['id'].'.dat'), $_input);
                    //return dd($input);
                    return view('checkout', $input);
                }
                // Если нажата кнопка подтверждения заказа
                else if(isset($input['confirm'])){

                    // Читаем текстовые данные заказа
                    $_input = file_get_contents(storage_path('app\public\order_'.$input['id'].'.dat'));
                    $fileInput = unserialize($_input);
                    
                    // Загружаем в память содержимое фотографий
                    // Объектов
                    if (isset($input['objectPhotos']))
                    foreach ($fileInput['objectPhotos'] as $k => $p) {
                        $objectPhotos[$k] = file_get_contents($p);
                    }
                    // Оружий
                    if (isset($input['gunPhotos']))
                    foreach ($fileInput['gunPhotos'] as $k => $p) {
                        $gunPhotos[$k] = file_get_contents($p);
                    }
                    // Декораций
                    if (isset($input['decorationPhotos']))
                    foreach ($fileInput['decorationPhotos'] as $k => $p) {
                        $decorationPhotos[$k] = file_get_contents($p);
                    }

                    return dd($fileInput['objectPhotos']);

                    // Добавляем заказ в базу
                    //return dd($fileInput);
                    DB::insert('INSERT INTO `orders` (`order_id`,`TYPE_STATUS_ID`) VALUES (\''.$input['id'].'\',\'1\')');// Id
                    
                    // Переходим на главную
                    return redirect('/');
                }
                // Если нажата кнопка отмены
                else if(isset($input['cancel'])){
                    return $this->makeOrder();
                }          
            }
            else
                abort(404);
        }
        else
            return redirect('/');
    }

    private function uploadFiles($files, $dest, $prefix = '', $count){
        $filenames = [];

        if (empty($files))
            return;

        $fCount = 0;
        foreach ($files as $f) {
            array_push($filenames, $dest.'\\'.$prefix.'_'.$fCount.'_'.$f->getClientOriginalName());
            $f->move($dest, $prefix.'_'.$fCount.'_'.$f->getClientOriginalName());
            ++$fCount;
            if ($fCount==$count)
                break;
        }
        return $filenames;
    }

    
    
}

