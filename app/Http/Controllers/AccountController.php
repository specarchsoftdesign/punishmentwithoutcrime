<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAccountRequest;
use App\Http\Requests\LogInRequest;
use Illuminate\Support\Facades\DB;
use Request;
use App\Models\Account;
use App\Models\Client;
use \Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Validator;
use Log;

class AccountController extends Controller
{
    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/company_profile';
    protected function guard()
    {
        return Auth::guard('guard-name');
    }
    /**
     * Регистрация нового пользователя.
     */
    public function register(CreateAccountRequest $request)
    {
        // Добавить введенные данные в БД
        $input = Request::all();

        $account = new Account;
        $account->TYPE_ACCOUNT_ID = 1;
        $account->login = $input['login'];
        $account->setPasswordAttribute($input['password']);
        $account->save();

        $client = new Client;
        $client->LAST_NAME = $input['last-name'];
        $client->FIRST_NAME = $input['first-name'];
        $client->PATHRONYMIC = $input['pathronymic'];
        $client->PHONE_NUMBER = str_replace(" ","",$input['number-phone']);
        $client->BIRTH_DATE = date("Y-m-d",strtotime($input['data-of-birth']));
        $client->ACCOUNT_ID = $account['account_id'];
        $client->DATE_REGISTRATION = Carbon::now();
        $client->save();

        session(['user' => Auth::user()]); // запоминаем пользователя сессии
        if (Auth::attempt(['login' => $input['login'],'password' => $input['password']])) {
            //Log::info('ID ');
            Log::info(Auth::id());
            //dd(Auth::id());
            if (Auth::check()){
                 session(['user' => Auth::user()]); // запоминаем пользователя сессии
                 //dd(Auth::user());
                 //if(session('user')['TYPE_ACCOUNT_ID']==1){
                if(Auth::user()['TYPE_ACCOUNT_ID']==1 || Auth::user()['TYPE_ACCOUNT_ID']==4){
                    //dd(Auth::user());
                    return redirect('main');
                }
                else if(Auth::user()['TYPE_ACCOUNT_ID']==2){
                    //dd(Auth::user());
                    return redirect('main_firm');
                }
                else if(Auth::user()['TYPE_ACCOUNT_ID']==3){
                    //dd(Auth::user());
                    return redirect('main_police');
                }
                else
                    abort(404);
            }
            else{
               abort(404);
            }
        }
        // Сделать переадресацию на главную страницу сайта
        return redirect('main'); 
    }

    /**
     * Авторизация.
     */
     public function logIn(LogInRequest $request)
     {
         $input = Request::all();
         /*Log::info('INPUT');
         Log::info($input);*/
         //dd($input);
         if (Auth::attempt(['login' => $input['login'],'password' => $input['password']])) {
            //Log::info('ID ');
            //Log::info(Auth::id());
            //dd(Auth::id());
            if (Auth::check()){
                 session(['user' => Auth::user()]); // запоминаем пользователя сессии
                 //dd(Auth::user());
                 //if(session('user')['TYPE_ACCOUNT_ID']==1){
                if(Auth::user()['TYPE_ACCOUNT_ID']==1){
                    //dd(Auth::user());
                    return redirect('main');
                }
                else if(Auth::user()['TYPE_ACCOUNT_ID']==2){
                    //dd(Auth::user());
                    return redirect('main_firm');
                }
                else if(Auth::user()['TYPE_ACCOUNT_ID']==3){
                    //dd(Auth::user());
                    return redirect('main_police');
                }
                else
                    abort(404);
            }
            else{
               abort(404);
            }
        }
        else{
            // Аутентификация не прошла - выводим сообщение об ошибке
            $validator = Validator::make($input, []);
            $validator->errors()->add('password', 'Введены неверные данные аккаунта.');
            return redirect('/')->withErrors($validator)->withInput(Request::except('password'));
        }
        
     }

     /**
     * Выход из аккаунта.
     */
     public function logOut()
     {
        Auth::logout();
        session()->forget('user');
        return redirect('/');
     }

}
