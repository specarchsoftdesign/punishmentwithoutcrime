<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            //пока только для клиента
            if(Auth::user()->TYPE_ACCOUNT_ID == 1)
                return redirect('main');
            else if(Auth::user()->TYPE_ACCOUNT_ID == 2)
                return redirect('main_firm');
            else if(Auth::user()->TYPE_ACCOUNT_ID == 3)
                return redirect('main_police');
        }

        return $next($request);
    }
}
