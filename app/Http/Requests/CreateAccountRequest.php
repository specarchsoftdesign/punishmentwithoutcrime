<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use \Datetime;

class CreateAccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $minDate = new DateTime();
        $minDate->modify('-16 year');
        return [
            'first-name' => 'required|between:2,20|regex:/^[А-Яа-яЁё]+$/u',
            'last-name' => 'required|between:2,20|regex:/^[А-Яа-яЁё]+$/u',
            'pathronymic' => 'required|between:5,20|regex:/^[А-Яа-яЁё]+$/u',
			'number-phone' => 'required|unique:Client,PHONE_NUMBER',
            'data-of-birth' => 'required|date|before:' . $minDate->format('d.m.Y'),
            'login' => 'required|between:3,20|unique:Account,LOGIN|regex:/^[A-Za-z0-9]+$/u',
			'password' => 'required|between:6,20|regex:/^[A-Za-z0-9]+$/u',
            'confirm-password' => 'required|same:password'
        ];
    }
}
