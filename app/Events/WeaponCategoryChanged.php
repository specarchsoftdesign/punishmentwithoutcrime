<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class WeaponCategoryChanged implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    protected $id = [];
    protected $name = [];
    protected $description = [];
    protected $image = [];

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($category)
    {
        // В зависимости от категории объекта нужно выбрать соответствующие записи из БД

        // Пока просто тестовое оружие
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('objects');
    }

    public function broadcastWith()
    {
        // This must always be an array. Since it will be parsed with json_encode()
        return [
            'id' => $this->id,
            'description' => $this->description,
            'image' => $this->image,
        ];
    }
}
