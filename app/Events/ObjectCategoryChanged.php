<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ObjectCategoryChanged implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    protected $orders;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($category)
    {
        // В зависимости от категории объекта нужно выбрать соответствующие записи из БД
        if($category == 0){
            $this->orders = DB::select('select obj.object_id as id
                                 ,inan.name
                                 ,inan.description
                                 ,ph.photo_1 as img 
                                 from object obj 
                                 natural join inanimate inan
                                 left join photo ph on inan.PHOTO_ID = ph.photo_id
                                 where obj.TYPE_OBJECT_ID =1 and obj.STANDART_OBJ_FLAG = 1' 
             );
         }
         else if($category == 1){
             $this->orders = DB::select('select obj.object_id as id
                                 ,an.kind as name
                                 ,an.description
                                 ,ph.photo_1 as img 
                                 from object obj 
                                 natural join animal an
                                 left join photo ph on an.PHOTO_ID = ph.photo_id
                                 where obj.TYPE_OBJECT_ID =2 and obj.STANDART_OBJ_FLAG = 1' 
             );
         }
         else if($category == 2){
             $this->orders = DB::select('select obj.object_id as id
                                 ,concat_ws(` `,h.SECOND_NAME,h.FIRST_NAME,h.PATHRONYMIC) as name
                                 ,concat_ws(`\n`,concat(`Пол: `,ifnull(h.gender,`x`))
                                             ,concat(`Возраст:`,ifnull(h.age,`x`))
                                             ,concat(`Рост: `,ifnull(h.GROWTH,`x`))
                                             ,concat(`Вес: `,ifnull(h.WEIGHT,`x`))
                                             ,concat(`Цвет глаз: `,ifnull(h.EYE_COLOR,`x`))
                                             ,concat(`Цвет волос: `,ifnull(h.HAIR_COLOR,`x`))
                                             ,h.DESCRIPTION) as description
                                 ,ph.photo_1 as img 
                                 from object obj 
                                 natural join human h
                                 left join photo ph on h.PHOTO_ID = ph.photo_id
                                 where obj.TYPE_OBJECT_ID =3 and obj.STANDART_OBJ_FLAG = 1' 
             );
         }
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('objects');
    }

    public function broadcastWith()
    {
        // This must always be an array. Since it will be parsed with json_encode()
        return [
            'orders' => $this->orders,
        ];
    }
}
