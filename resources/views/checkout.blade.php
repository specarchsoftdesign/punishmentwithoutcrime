<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	
    <title>Оформить заказ</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
	
    <!-- Main Style -->
    <link href="css/style.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Kurale" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Lora" rel="stylesheet">
  </head>
  <body>
  	<div class="bg-checkout col-lg-12">
		<img src="images/bg-checkout.jpg"> 
	</div>
	<div class="head col-lg-12">
		<h1>КИЛЛ & МОЛЛ</h1> 
	</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<div class="menu col-lg-12">
		<div class="marker-menu col-lg-3">
			<a href="main" class="btn btn-default">О компании</a>
		</div>
		<div class="marker-menu col-lg-3">
			<a href="profile" class="btn btn-default">Профиль</a>
		</div>
		<div class="marker-menu col-lg-3">
			<a href="order_of_services" class="btn btn-default">Заказ услуг</a>
		</div>
		<div class="marker-menu col-lg-3">
			<a href="logout" class="btn btn-default">Выйти</a>
		</div>
	</div>
	<div class="checkout col-lg-12">
		<div class="check col-lg-4 col-lg-offset-4" id="client-check"> 
			<form action="order_of_services" method="POST" enctype="">
			<input type="hidden" name="id" value="{{$id}}">
			{{ csrf_field() }}
				<h3>K&M</h3>
				<h4>Ваш заказ №: {{$id}}  </h4>
				<p>Объект:</p>
				<p>Категория: {{$object_category}} </p>
				<p>Описание: 
				@if ($object_category == 'человек')
					<br/> - имя: {{$first_name}}
					<br/> - фамилия: {{$second_name}}
					<br/> - отчество: {{$pathronymic}}
					<br/> - пол: {{$gender}}
					<br/> - возраст: {{$age}}
					<br/> - рост: {{$growth}}
					<br/> - вес: {{$weight}}					
					<br/> - цвет глаз: {{$eyes_color}}
					<br/> - цвет волос: {{$hair_color}} 
				@elseif ($object_category == 'неодушевленный')
					<br/> - название: {{$name_inanimate}}
				@else
					<br/> - вид: {{$type_animal}}
				@endif
					<br/> {{$object_description}} 
				</p>
				<p>Стоимость: {{$object_price}} </p>

				<p>Орудие:</p>
				<p>Категория: {{$weapon_category}}  </p>
				<p>Описание:  {{$weapon_description}} </p>
				<p>Стоимость: {{$weapon_price}} </p>
			
				<p>Декорация:</p>
				<p>Категория: {{$decoration_category}}   </p>
				<p>Описание: {{$decoration_description}} </p>
				<p>Стоимость: {{$decoration_price}} </p>

				<p>Дата: {{$murder_date}} </p>
				<p>Общая сумма:<strong> {{$common_price}} </strong></p>
				<hr>
				<div class="btn-checkout col-lg-12">
					<button type="submit" name="confirm" class="btn btn-primary" value='1'>Подтвердить</button>
					<button type="submit" name="cancel" class="btn btn-primary" value='1'>Отменить</button>
				</div>
			</form>
		</div>
	</div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>