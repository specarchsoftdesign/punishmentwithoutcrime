<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Настройки</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
	
    <!-- Main Style -->
    <link href="css/style.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Kurale" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora" rel="stylesheet">
  </head>
  <body>
    <div class="bg-about-content col-lg-12">
        <img src="images/bg-about-services.jpg">
    </div>
    <div class="head col-lg-12">
        <h1>КИЛЛ & МОЛЛ</h1> 
    </div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<div class="menu col-lg-12">
		<div class="marker-menu col-lg-3">
			<a href="main_police" class="btn btn-default">О компании</a>
		</div>
		<div class="marker-menu col-lg-3">
			<a href="history_orders_police" class="btn btn-default">История заказов</a>
		</div>
		<div class="marker-menu col-lg-3">
			<a href="#" class="btn btn-default">Настройки</a>
		</div>
		<div class="marker-menu col-lg-3">
			<a href="logout" class="btn btn-default">Выйти</a>
		</div>
	</div>
    
        <form action="setting_police" method="POST" enctype="multipart/form-data">
        <div class="setting-content-pravoorg col-lg-12">
        <h3>Требования для тюремного заключения</h3>
	    {{ csrf_field() }}
        <ul class="list-setting col-lg-11">
            <div class="first-setting col-lg-7">
                <li>Количесво заказов неодушевленных объектов:</li>
                <input type="text" class="form-control" name="" placeholder="" value="">
            </div>
            <div class="second-setting col-lg-7">
                 <li>Количество заказов животных:</li>
                <input type="text" class="form-control" name="" placeholder="" value="">
            </div>
            <div class="third-setting col-lg-7">
                <li>Количество заказов людей:</li>
                <input type="text" class="form-control" name="" placeholder="" value="">
            </div>
        </ul>
        <div class="btn-setting-pravoohran col-lg-12">
           <button type="submit" name="btn-setting-pravoohran" class="btn btn-primary">Применить</button>
        </div>
        </div>
        </form>
    <script src="js/bootstrap.min.js"></script>
	<script src="js/maskedinput.js"></script>
  </body>
</html>