<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Информация о заказе</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
	
    <!-- Main Style -->
    <link href="css/style.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Kurale" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Lora" rel="stylesheet">
  </head>
  <body>
  
	<div class="head col-lg-12">
		<h1>КИЛЛ & МОЛЛ</h1> 
	</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<div class="menu col-lg-12">
		<div class="marker-menu col-lg-3">
			<a href="main_police" class="btn btn-default">О компании</a>
		</div>
		<div class="marker-menu col-lg-3">
			<a href="history_orders_police" class="btn btn-default">История заказов</a>
		</div>
		<div class="marker-menu col-lg-3">
			<a href="setting_police" class="btn btn-default">Настройки</a>
		</div>
		<div class="marker-menu col-lg-3">
			<a href="logout" class="btn btn-default">Выйти</a>
		</div>
	</div>
	<div class="firm-orderhist-content col-lg-12">
	<img src="images/bg-info-order-org.jpg">
		<div class="history col-lg-12">
		<h3>Информация о заказе клиента</h3>
			<div id="info-client" class="inform-client col-lg-6">
			<h4>Клиент:</h4>
				<form action="form-table" method="" enctype="">
					<div class="table-info-client" id="tab-infclient">
					  <table class="table table-bordered">
						<tr>
							<td class="text-center">Уровень аккаунта:</td>
							<td><!--уровень аккаунта--></td>
						</tr>
						<tr>
							<td class="text-center">ФИО:</td>
							<td><!--фио--></td>
						</tr>
						<tr>
							<td class="text-center">Номер телефона:</td>
							<td><!--номер телефона--></td>
						</tr>
						<tr>
							<td class="text-center">День рождения:</td>
							<td><!--день рождения--></td>
						</tr>
					 </table>
					</div>				
				</form>
			</div>
			<div id="info-order" class="inform-order col-lg-6">
			<h4>Заказ:</h4>
				<form action="form-table" method="" enctype="">
					<div class="table-info-order" id="tab-inford">
					  <table class="table table-bordered">
						<tr>
							<td class="text-center">Номер заказа:</td>
							<td><!--номер заказа--></td>
						</tr>
						<tr>
							<td class="text-center">Дата:</td>
							<td><!--дата--></td>
						</tr>
						<tr>
							<td class="text-center">Статус:</td>
							<td><!--статус--></td>
						</tr>
						<tr>
							<td class="text-center">Информация об объекте:</td>
							<td><!--информация об объекте--></td>
						</tr>
						<tr>
							<td class="text-center">Информация об оружии:</td>
							<td><!--информация об оружии--></td>
						</tr>
												<tr>
							<td class="text-center">Информация о декорации:</td>
							<td><!--информация о декорации--></td>
						</tr>
					 </table>
					</div>				
				</form>
			</div>
			<div class="but col-lg-12">
				<p><a href="" class="btn btn-default">Вернуться назад</a><p>
			</div>
		</div>
	</div>
    <script src="js/bootstrap.min.js"></script>
	<script src="js/maskedinput.js"></script>
  </body>
</html>