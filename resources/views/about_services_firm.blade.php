<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		
    <title>О компании</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
	
    <!-- Main Style -->
    <link href="css/style.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Kurale" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Lora" rel="stylesheet">
  </head>
  <body>
  	<div class="bg-about-content col-lg-12">
		<img src="images/bg-about-services.jpg">
	</div>
	<div class="head col-lg-12">
		<h1>КИЛЛ & МОЛЛ</h1> 
	</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<div class="menu col-lg-12">
		<div class="marker-menu col-lg-3">
			<a href="main_firm" class="btn btn-default">О компании</a>
		</div>
		<div class="marker-menu col-lg-3">
			<a href="history_orders_firm" class="btn btn-default">История заказов</a>
		</div>
		<div class="marker-menu col-lg-3">
			<a href="#" class="btn btn-default">Настройки</a>
		</div>
		<div class="marker-menu col-lg-3">
			<a href="logout" class="btn btn-default">Выйти</a>
		</div>
	</div>
	<div class="about-content col-lg-12">
		<h3>О компании</h3>
			<p>Наша компания предоставляет возможность заказа услуг по вымещению гнева путем уничтожения виртуальных объектов.</p>
			<p>Вы можете выбрать объект, орудие и декорацию для убийства из предлагаемого нами перечня или же загрузить свой вариант идеального убийства для осуществления своей сокровенной мечты.</p>
		<h4>Уровни аккаунтов</h4>
			<li>1 уровень - возможен заказ только неодушевленных предметов;</li>
			<li>2 уровень - помимо неодушевленных предметов возможен заказ животных;</li>
			<li>3 уровень - помимо неодушевленных предметов и животных возможен заказ людей.</li>
		<h4>Требования для повышения аккаунта</h4>
			<p>Чтобы повысить уровень нужно сделать определенное количество заказов со своего аккаунта. 
			1 уровень - доступен сразу же после регистрации; 2 уровень - приобретается после 5 заказов; 
			3 уровень - приобретается после 15 заказов.</p>
			<h4>Как нас найти</h4>
			<div id="map-firm" class="map col-lg-6 col-lg-offset-3"></div>
	</div>
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
		<script src="js/map.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>