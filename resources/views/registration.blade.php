<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Регистрация</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
	
    <!-- Main Style -->
    <link href="css/style.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Kurale" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Lora" rel="stylesheet">
  </head>
  <body>
  <div class="bg-registration col-lg-12">
  	<img src="images/bg-registration.jpg">
</div>
	<div class="head col-lg-12">
		<h1>КИЛЛ & МОЛЛ</h1> 
	</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<div class="registration-content col-lg-12">
		<div class="registration col-lg-4 col-lg-offset-4">
			<h1>Регистрация</h1>
			<form method="POST" action="registration" class="navbar-form" accept-charset="UTF-8">
			{{ csrf_field() }}			
				<div class="form-group">
					@if ($errors->any())
						<ul class="alert alert-danger">
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					@endif
					<label>Фамилия:</label>
					<input type="text" class="form-control" name="last-name" placeholder="" value="">
					<label>Имя:</label>
					<input type="text" class="form-control" name="first-name" placeholder="" value="">
					<label>Отчество:</label>
					<input type="text" class="form-control" name="pathronymic" placeholder="" value="">
					<label>Номер телефона:</label>
					<input type="text" id="phone" class="form-control" name="number-phone" placeholder="" value="">
					<script>
						jQuery(function($){
							$("#phone").mask("+7(999) 999-9999");
						})
					</script>
					<label>Дата рождения:</label>
					<input type="date" class="form-control" name="data-of-birth" placeholder="" value="">
					<label>Логин:</label>
					<input type="text" class="form-control" name="login" placeholder="" value="">
					<label>Пароль:</label>
					<input type="password" class="form-control" name="password" placeholder="" value="">
					<label>Подтвердите пароль:</label>
					<input type="password" class="form-control" name="confirm-password" placeholder="" value="">
				</div>
				<button type="submit" name="reg-client" class="btn btn-primary">Зарегистрироваться</button>
			</form>
			<li><a href="/">Вернуться назад</a></li>
		</div>
	</div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/bootstrap.min.js"></script>
	<script src="js/maskedinput.js"></script>
  </body>
</html>