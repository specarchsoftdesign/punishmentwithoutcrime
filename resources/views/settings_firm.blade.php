<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Настройки</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
	
    <!-- Main Style -->
    <link href="css/style.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Kurale" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora" rel="stylesheet">
  </head>
  <body>
    <div class="bg-setting col-lg-12">
        <img src="images/bg-setting-firm.png">
    </div>
    <div class="head col-lg-12">
        <h1>КИЛЛ & МОЛЛ</h1> 
    </div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<div class="menu col-lg-12">
		<div class="marker-menu col-lg-3">
			<a href="main_firm" class="btn btn-default">О компании</a>
		</div>
		<div class="marker-menu col-lg-3">
			<a href="history_orders_firm" class="btn btn-default">История заказов</a>
		</div>
		<div class="marker-menu col-lg-3">
			<a href="#" class="btn btn-default">Настройки</a>
		</div>
		<div class="marker-menu col-lg-3">
			<a href="logout" class="btn btn-default">Выйти</a>
		</div>
	</div>
    <div class="setting-content-firm col-lg-12">
        <h3>Требования для достижения 2 уровня</h3>
        <ul class="list-setting col-lg-11">
            <div class="first-setting col-lg-7">
                <li>Количесво заказов:</li>
                <input type="text" class="form-control" name="" placeholder="" value="">
            </div>
        </ul>
        <h3>Требования для достижения 3 уровня</h3>
        <ul class="list-setting col-lg-11">
            <div class="first-setting col-lg-7">
                <li>Количесво заказов:</li>
                <input type="text" class="form-control" name="" placeholder="" value="">
            </div>
        </ul>
        <div class="btn-setting-firm col-lg-12">
           <button type="submit" name="btn-setting-firm" class="btn btn-primary">Применить</button>
        </div>
    </div>
    <script src="js/bootstrap.min.js"></script>
	<script src="js/maskedinput.js"></script>
  </body>
</html>