<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		
    <title>Профиль</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
	
    <!-- Main Style -->
    <link href="css/style.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Kurale" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Lora" rel="stylesheet">
  </head>
  <body>
  
	<div class="head col-lg-12">
		<h1>КИЛЛ & МОЛЛ</h1> 
	</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<div class="menu col-lg-12">
		<div class="marker-menu col-lg-3">
			<a href="main" class="btn btn-default">О компании</a>
		</div>
		<div class="marker-menu col-lg-3">
				<a href="profile" class="btn btn-default">Профиль</a>
		</div>
		<div class="marker-menu col-lg-3">
				<a href="order_of_services" class="btn btn-default">Заказ услуг</a>
		</div>
		<div class="marker-menu col-lg-3">
				<a href="logout" class="btn btn-default">Выйти</a>
		</div>
	</div>
	<div class="client-prof-content col-lg-12">
			<img src="images/bg-client-prof.jpg">
			<div class="client-profile col-lg-12">
					<h3>Профиль</h3>
					<div class="profile-info col-lg-5" id="info-client">
							<p>ФИО: {{ $name }} </p>
							<p>Номер телефона: {{ $phone }}</p>
							<p>Дата рождения: {{ $date }} </p>
							<p>Статус аккаунта: {{ $status }} </p>
							<!--<p>Уровень аккаунта: </p>
							<p>Количество заказов: </p>-->
					</div>
					<div class="police-buttons col-lg-3 col-lg-offset-2">
							<button type="submit" name="btn-butpolice" class="btn btn-primary">Посадить в тюрьму</button>
							<button type="submit" name="btn-butpolice" class="btn btn-primary">Амнистировать</button>
					</div>
					<a href="client_history" class="btn btn-default">История заказов</a>
			</div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/bootstrap.min.js"></script>
	<script src="js/maskedinput.js"></script>
  </body>
</html>