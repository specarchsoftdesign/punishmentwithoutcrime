<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">
    <title>История заказов</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
	
    <!-- Main Style -->
    <link href="css/style.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Kurale" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Lora" rel="stylesheet">
  </head>
  <body>
      <div class="bg-history-pravoorgan">
		<img src="images/bg-history-pravoorgan.png"> 
	</div>
	<div class="head col-lg-12">
		<h1>КИЛЛ & МОЛЛ</h1> 
	</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<div class="menu col-lg-12">
		<div class="marker-menu col-lg-3">
			<a href="main_police" class="btn btn-default">О компании</a>
		</div>
		<div class="marker-menu col-lg-3">
			<a href="history_orders_police" class="btn btn-default">История заказов</a>
		</div>
		<div class="marker-menu col-lg-3">
			<a href="setting_police" class="btn btn-default">Настройки</a>
		</div>
		<div class="marker-menu col-lg-3">
			<a href="logout" class="btn btn-default">Выйти</a>
		</div>
	</div>
	<div class="history col-lg-12">
		<h3>История заказов</h3>
		<h4>Заказы клиентов:</h4>
		<div class="filter col-lg-11 col-lg-offset-1">
		<form action="history_orders_police" method="POST" enctype="">
			{{ csrf_field() }}
			<div class="buttons-filter col-lg-5 col-lg-offset-7">
				<button type="submit" name="btn-apply-filter" value="1" class="btn btn-primary">Применить фильтры</button>
				<button type="submit" name="btn-reset-filter" value="1" class="btn btn-primary">Сбросить фильтры</button>
			</div>
			<div class="login-filter" id="search-login">
				<label for="edit-filter-login">Логин</label>
				<input type="text" name="input-filter-login" id="edit-filter-login" class="form-control"></input>
			</div>
			<div class="fio-filter" id="search-fio">
				<label for="edit-filter-fio">ФИО</label>
				<input type="text" name="input_filter_fio" id="edit-filter-fio" class="form-control"></input>
			</div>
			<div class="typeobj-filter" id="search-typeobj">
				<label for="edit-filter-typeobj">Тип объета</label>
				<select name="input-filter-typeobj" id="edit-filter-typeobj" class="form-control">
					<option selected>--</option>
					<option value="1">неодушевленный</option>
					<option value="2">животное</option>
					<option value="3">человек</option>
				</select>
			</div>
			<div class="price-filter" id="search-price">
				<label>Стоимость</label>
				<input type="text" name="input-filter-price-min" id="edit-filter-price-1" placeholder="min" class="form-control"></input>
				<input type="text" name="input-filter-price-max" id="edit-filter-price-2" placeholder="max" class="form-control"></input>
			</div>
			<div class="date-filter" id="search-date">
				<label>Дата заказа</label>
				<input type="text" name="input-filter-date-min" id="edit-filter-date-1" placeholder="от" class="form-control"></input>
				<input type="text" name="input-filter-date-max" id="edit-filter-date-2" placeholder="до" class="form-control"></input>
			</div>
			<div class="status-filter" id="search-status">
				<label for="edit-filter-status">Статус</label>
					<select name="input-filter-order-status" id="edit-filter-status" class="form-control">
						<option selected>--</option>
						<option value="1">в рассмотрении</option>
						<option value="2">одобрен</option>
						<option value="3">завершен</option>
						<option value="4">отменен</option>
					</select>
			</div>
			<div class="account-status-filter" id="search-account-status">
				<label for="edit-filter-account-status">Состояние аккаунта</label>
					<select name="input-filter-acc-status" id="edit-filter-account-status" class="form-control">
						<option selected>--</option>
						<option value="1">активен</option>
						<option value="2">закрыт</option>
					</select>
			</div>
		</div>
		<div id="tab-organ" class="content-history col-lg-12">
			<!--<form action="history_orders_police" method="POST" enctype="">
			{{ csrf_field() }}-->
				<div class="table" id="hist-organ" style="overflow-x:auto">
				  <table class="table table-bordered">
					  <thead>
						  <tr>
							<td class="text-center"></td>
							<td class="text-center">№</td>
							<td class="text-center">Логин</td>
							<td class="text-center">ФИО</td>
							<td class="text-center">Тип объекта</td>
							<td class="text-center">Стоимость</td>
							<td class="text-center">Дата заказа</td>
							<td class="text-center">Статус</td>
							<td class="text-center">Состояние заказа</td>
						  </tr>
					 </thead>
					 <tbody align="center">
						 @foreach ($orders as $order)
						<tr>
							<td><input type="checkbox"  name="orders_id[]" value="{{ $order->ORDER_ID }}"></td>
							<td>{{$order->ORDER_ID}}</td>
							<td>{{$order->login}}</td>
							<td>{{$order->fio}}</td>
							<td>{{$order->type_object_name}}</td>
							<td>{{$order->AMOUNT}}</td>
							<td>{{$order->DATE_ORDER}}</td>
							<td>{{$order->status_name}}</td>
							<td>{{$order->account_status}}</td>
						</tr>
						@endforeach
					 </tbody>
				   </table>
				</div>
				<div class="buttons-pravoorgan col-lg-12">
					<button type="submit" name="btn-organ" value="1" class="btn btn-primary">Задержать</button>
					<button type="submit" name="btn-butpolice" value="1" class="btn btn-primary">Амнистировать</button>
				</div>
			</form>
		</div>
	</div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/bootstrap.min.js"></script>
	<script src="js/maskedinput.js"></script>
  </body>
</html>