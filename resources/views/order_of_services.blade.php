<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Заказ услуг</title>
  
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
	
    <!-- Main Style -->
    <link href="css/style.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Kurale" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Lora" rel="stylesheet">
  </head>
  <body>
  <div id='app'></div>
      <div class="bg-order-of-services">
		<img src="images/bg-order-of-services.jpg"> 
	</div>
	<div class="head col-lg-12">
		<h1>КИЛЛ & МОЛЛ</h1> 
	</div>
	<div class="menu col-lg-12">
		<div class="marker-menu col-lg-3">
			<a href="main" class="btn btn-default">О компании</a>
		</div>
		<div class="marker-menu col-lg-3">
			<a href="profile" class="btn btn-default">Профиль</a>
		</div>
		<div class="marker-menu col-lg-3">
			<a href="order_of_services" class="btn btn-default">Заказ услуг</a>
		</div>
		<div class="marker-menu col-lg-3">
			<a href="logout" class="btn btn-default">Выйти</a>
		</div>
	</div>
	<form action="order_of_services" method="POST" enctype="multipart/form-data">
	{{ csrf_field() }}
		<div class="order-of-services col-lg-12">
			<h3>Заказ услуг</h3>
			<!-- <form action="order_of_services" method="POST" enctype=""> -->
			<div class="select-object col-lg-10">
				<div class="sel-obj-part col-lg-3">
					<h4>Выбор объекта:</h4>
					<label for="edit-select-obj">Выбор категории:</label>
					<select id="edit-select-obj" name ="object_category" class="form-control">
					@foreach ($types_object as $type)
						<option value="{{$type->NAME}}">{{$type->NAME}}</option>
					@endforeach
					</select>
					<label id="object_price">Стоимость:</label>
				</div>
				<div class="radio-btn col-lg-12">
					<input type="radio" id="table-list" value="table-object" name="object-radio-list" checked>
					<label>Выбрать объект из предлагаемого перечня</label> 
				</div>  
				<div class="radio-btn col-lg-12">	            
					<input type="radio" id="my-object" value="custom-object" name="object-radio-list">
					<label>Предложить свой объект</label>     
				</div>
				<div class="table-select-obj col-lg-12" id="tab-selobj">
					<!-- <form action="" method="" enctype=""> -->
						<div class="scroll-table">
							<table class="table table-bordered" id="objects-table">
								<thead>
									<tr>
										<td class="text-center"></td>
										<td class="text-center">Название</td>
										<td class="text-center">Описание</td>
										<td class="text-center">Изображение</td>
									</tr>
								</thead>
								<tbody align="center">
									<tr>
									</tr>
								</tbody>
							</table>
						</div>	 
					<!-- </form> -->
				</div>
				<div class="content-select-obj col-lg-12">
					<div class="dop-inform-obj col-lg-4">
						<label>Дополнительное описание:</label>
						<textarea class="form-control" name="object_description" id="dop-inf-obj"></textarea>
					</div>
					<div class="hidden-inform col-lg-8">
					<!--если выбрана категория человек-->
						<div class="navbar-form form-person">
							<div class="first-column col-lg-6" id="first-person-info">
								<div class="inform-person col-lg-12">
									<div class="name-inform-row col-lg-4">
										<label>Имя:</label>
									</div>
									<input type="text" class="form-control col-lg-8" name="first_name" value="">
								</div>
								<div class="inform-person col-lg-12">
									<div class="name-inform-row col-lg-4">
										<label>Фамилия:</label>
									</div>
									<input type="text" class="form-control" name="second_name" value="">
								</div>
								<div class="inform-person col-lg-12">
									<div class="name-inform-row col-lg-4">
										<label>Отчество:</label>
									</div>
									<input type="text" class="form-control" name="pathronymic" value="">
								</div>
								<div class="inform-person col-lg-12">
									<div class="name-inform-row col-lg-4">
										<label>Пол:</label>
									</div>
									<input type="text" class="form-control" name="gender" value="">
								</div>						
								<div class="inform-person col-lg-12">
									<div class="name-inform-row col-lg-4">
										<label>Возраст:</label>
									</div>
									<input type="text" class="form-control" name="age" value="">
								</div>
							</div>
							<div class="second-column col-lg-6" id="second-person-info">
								<div class="inform-person col-lg-12">
									<div class="name-inform-row col-lg-4">
										<label>Рост:</label>
									</div>
									<input type="text" class="form-control" name="growth" value="">
								</div>	
								<div class="inform-person col-lg-12">
									<div class="name-inform-row col-lg-4">
										<label>Вес:</label>
									</div>
									<input type="text" class="form-control" name="weight" value="">
								</div>
								<div class="inform-person col-lg-12">
									<div class="name-inform-row col-lg-4">
										<label>Цвет глаз:</label>
									</div>
									<input type="text" class="form-control" name="eyes_color" value="">
								</div>
								<div class="inform-person col-lg-12">
									<div class="name-inform-row col-lg-4">
										<label>Цвет волос:</label>
									</div>
									<input type="text" class="form-control" name="hair_color" value="">
								</div>		
							</div>					
						</div>	
					<!--если выбрана категория неодушевленный объект-->
					<div class="navbar-form form-inanimate">
						<div class="inform-inanimate col-lg-12">
							<div class="name-inform-row col-lg-3">
								<label>Название:</label>
							</div>
							<input type="text" class="form-control" name="name_inanimate" value="">
						</div>
					</div>
					<!--если выбрана категория животное-->
					<div class="navbar-form form-animal">
						<div class="inform-animal col-lg-12">
							<div class="name-inform-row col-lg-3">
								<label>Вид:</label>
							</div>
							<input type="text" class="form-control" name="type_animal" value="">
						</div>
					</div>
				</div>
				<!-- <form class="download-img-obj col-lg-10" enctype="multipart/form-data" method="post"> -->
					<div class="form-group download-img-obj col-lg-10">
						<label for="file">Загрузить изображения своего объекта (доступна загрузка до 3-х фотографий):</label>
						<input type="file" accept="image/jpeg,image/png" id="files" name="files[]" multiple />
						<output id="list"></output>
					</div>
				<!-- </form> -->
			</div>
		</div>
			<div class="select-gun col-lg-10">
				<div class="sel-gun-part col-lg-3">
					<h4>Выбор орудия:</h4>
					<label for="edit-select-gun">Выбор категории:</label>
					<select id="edit-select-gun" name="weapon_category" class="form-control">
						@foreach ($types_weapon as $type)
							<option value="{{$type->NAME}}">{{$type->NAME}}</option>
						@endforeach
					</select>
					<label id="weapon_price">Стоимость:</label>
				</div>
				<div class="radio-btn col-lg-12">
					<input type="radio" id="table-list" name="weapon-radio-list" value="table-weapon" checked>
                    <label>Выбрать орудие из предлагаемого перечня</label>
				</div>
				<div class="radio-btn col-lg-12">
					<input type="radio" id="my-object" name="weapon-radio-list" value="custom-weapon">
					<label>Предложить свое орудие</label>     
				</div>
				<div class="table-select-gun col-lg-12" id="tab-selgun">
					<!-- <form action="" method="" enctype=""> -->
						<div class="scroll-table">
							<table class="table table-bordered" id="weapons-table">
								<thead>
									<tr>
										<td class="text-center"></td>
										<td class="text-center">Описание</td>
										<td class="text-center">Изображение</td>
									</tr>
								</thead>
								<tbody align="center">
									<tr>
									</tr>
								</tbody>
							</table>
						</div>
					<!-- </form> -->
				</div>
				<div class="content-select-gun col-lg-12">
					<div class="dop-inform-gun col-lg-4">
						<label>Дополнительное описание:</label>
						<textarea class="form-control" name="weapon_description" id="dop-inf-gun"></textarea>
					</div>
					<!-- <form class="download-img-gun col-lg-10" enctype="multipart/form-data" method="post"> -->
						<div class="form-group download-img-gun col-lg-10">
							<label for="file">Загрузить изображения своего оружия (доступна загрузка до 3-х фотографий):</label>
							<input type="file" accept="image/jpeg,image/png" id="files-gun" name="files-gun[]" multiple />
							<output id="list-gun"></output>
						</div>
					<!-- </form> -->
				</div>
			</div>
			<div class="select-locale col-lg-10">
				<div class="sel-locale-part col-lg-5">
					<h4>Выбор декорации:</h4>
					<label for="edit-select-locale">Выбор категории:</label>
					<select id="edit-select-locale" name="decoration_category" class="form-control">
						@foreach ($types_decoration as $type)
							<option value="{{$type->NAME}}">{{$type->NAME}}</option>
						@endforeach
					</select>
					<label id="decoration_price">Стоимость:</label>
				</div>
				<div class="radio-btn col-lg-12">
					<input type="radio" id="table-list" name="decoration-radio-list" value="table-decoration" checked>
                    <label>Выбрать декорацию из предлагаемого перечня</label>
				</div>
				<div class="radio-btn col-lg-12">
					<input type="radio" id="my-object" name="decoration-radio-list" value="custom-decoration">
					<label>Предложить свою декорацию</label>     
				</div>
				<div class="table-select-locale col-lg-12" id="tab-sellocale">
					<!-- <form action="" method="" enctype=""> -->
						<div class="scroll-table">
							<table class="table table-bordered" id="decorations-table">
								<thead>
									<tr>
										<td class="text-center"></td>
										<td class="text-center">Описание</td>
										<td class="text-center">Изображение</td>
									</tr>
								</thead>
								<tbody align="center">
									<tr>
									</tr>
								</tbody>
							</table>
						</div>
					<!-- </form> -->
				</div>
				<div class="content-select-locale col-lg-12">
					<div class="dop-inform-locale col-lg-4">
						<label>Дополнительное описание:</label>
						<textarea class="form-control" name="decoration_description" id="dop-inf-locale"></textarea>
					</div>
					<!-- <form class="download-img-locale col-lg-10"  enctype="multipart/form-data" method="post"> -->
						<div class="form-group download-img-locale col-lg-10">
							<label for="file">Загрузить изображения своей декорации (доступна загрузка до 3-х фотографий):</label>
							<input type="file" accept="image/jpeg,image/png" id="files-locale" name="files-locale[]" multiple />
							<output id="list-locale"></output>
						</div>
					<!-- </form> -->
				</div>
			</div>
			<div class="table-select-date col-lg-2" id="select-date">
				<h4>Выбор даты:</h4>
				<input type="date" class="form-control" name="murder_date" placeholder="" value="">
			</div>
			<div class="but-order-services col-lg-12" id="but-order-serv">
				<button type="submit" value="1" name="order-serv" class="btn btn-primary">Оформить заказ</button>
			</div>
		</form>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/order.js"></script>
	<script src="js/app.js"></script>

	<!--Выбор файлов и чтение-->
	<script>
		function handleFileSelect(evt) {
			var files = evt.target.files;
			var result = 3 - document.querySelectorAll("#list span").length;
			if(result > 0){
				for (var i = 0; i < result;  i++) {
					var f = files[i];
					var reader = new FileReader();
					reader.onload = (function(theFile) {
						return function(e) {
							var span = document.createElement('span');
							span.innerHTML = ['<img class="thumb" src="', e.target.result,
												'" title="', escape(theFile.name), '"/>'].join('');
							document.getElementById('list').insertBefore(span, null);
						};
					})(f);
					reader.readAsDataURL(f);
				}
			}
			else {
				var b = document.querySelector("#files");
				b.disabled = true;
			}
  		}
		function handleFileSelectGun(evt) {
			var files = evt.target.files; 
			var result = 3 - document.querySelectorAll("#list-gun span").length;
			if(result > 0){
				for (var i = 0; i < result;  i++) {
					var f = files[i];
					var reader = new FileReader();
					reader.onload = (function(theFile) {
						return function(e) {
						var span = document.createElement('span');
						span.innerHTML = ['<img class="thumb" src="', e.target.result,
											'" title="', escape(theFile.name), '"/>'].join('');
						document.getElementById('list-gun').insertBefore(span, null);
						};
					})(f);
					reader.readAsDataURL(f);
				}
			}
			else {
				var b = document.querySelector("#files-gun");
				b.disabled = true;
			}
  		}
		function handleFileSelectLocale(evt) {
			var files = evt.target.files; 
			var result = 3 - document.querySelectorAll("#list-locale span").length;
			if(result > 0){
				for (var i = 0; i < result;  i++) {
					var f = files[i];
					var reader = new FileReader();
					reader.onload = (function(theFile) {
						return function(e) {
						var span = document.createElement('span');
						span.innerHTML = ['<img class="thumb" src="', e.target.result,
											'" title="', escape(theFile.name), '"/>'].join('');
						document.getElementById('list-locale').insertBefore(span, null);
						};
					})(f);
				reader.readAsDataURL(f);
				}
			}
			else {
				var b = document.querySelector("#files-locale");
				b.disabled = true;
			}
  		}
		document.getElementById('files').addEventListener('change', handleFileSelect, false);
		document.getElementById('files-gun').addEventListener('change', handleFileSelectGun, false);
		document.getElementById('files-locale').addEventListener('change', handleFileSelectLocale, false);
  </script>
  </body>
</html>