<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="csrf-token" content="{{ csrf_token() }}">
    <title>История заказов</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
	
    <!-- Main Style -->
    <link href="css/style.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Kurale" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Lora" rel="stylesheet">
  </head>
  <body>
  	<div class="bg-history-client">
		<img src="images/bg-history-client.jpg">
	</div>
	<div class="head col-lg-12">
		<h1>КИЛЛ & МОЛЛ</h1> 
	</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<div class="menu col-lg-12">
		<div class="marker-menu col-lg-3">
			<a href="main" class="btn btn-default">О компании</a>
		</div>
		<div class="marker-menu col-lg-3">
				<a href="profile" class="btn btn-default">Профиль</a>
		</div>
		<div class="marker-menu col-lg-3">
				<a href="order_of_services" class="btn btn-default">Заказ услуг</a>
		</div>
		<div class="marker-menu col-lg-3">
				<a href="logout" class="btn btn-default">Выйти</a>
		</div>
	</div>
	<div class="hist-client-content col-lg-12">
	<div class="history col-lg-12">
			<h3>История заказов</h3>
			<div id="tab-client" class="content-history col-lg-9 col-lg-offset-1">
				<form action="form-table" method="" enctype="">
				{{ csrf_field() }}
					<div class="table" id="hist-client" style="overflow-x:auto">
					  <table class="table table-bordered">
						  <thead>
							  <tr>
								<td class="text-center">№</td>
								<td class="text-center">Дата</td>
								<td class="text-center">Статус</td>
								<td class="text-center">Категория объекта</td>
								<td class="text-center">Стоимость</td>
								<td class="text-center"></td>
							  </tr>
						</thead>
						 <tbody align="center">
						 @foreach ($orders as $order)
							<tr>
								<td>{{$order->ORDER_ID}}</td>
								<td>{{$order->DATE_ORDER}}</td>
								<td>{{$order->NAME}}</td>
								<td>{{$order->type_object_name}}</td>
								<td>{{$order->AMOUNT}}</td>
								<td><a name="" href="">Подробнее</a></td>
							</tr>
							@endforeach
						 </tbody>
					  </table>
					</div>				
				</form>
			</div>
		</div>
	</div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/bootstrap.min.js"></script>
	<script src="js/maskedinput.js"></script>
  </body>
</html>