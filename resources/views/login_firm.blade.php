<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Главная страница</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
	
    <!-- Main Style -->
    <link href="css/style.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Kurale" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Lora" rel="stylesheet">
  </head>
  <body>
	<div class="head col-lg-12">
		<h1>КИЛЛ & МОЛЛ</h1> 
	</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<div class="menu-firm col-lg-12">
		<a href="main_firm" class="btn btn-default">О компании</a>
		<a href="" class="btn btn-default">История заказов</a>
		<a href="logout" class="btn btn-default">Выйти</a>
	</div>
	<div class="image-block col-lg-12">
		<div class="content-image col-lg-4">
			<img src="images/bg.png">
		</div>
		<div class="content-image-1 col-lg-4">
			<img src="images/bg-1.png">
		</div>
		<div class="content-image-2 col-lg-4">
			<img src="images/bg-2.png">
		</div>
	</div>
	<div class="warning col-lg-10 col-lg-offset-1">
		<hr>
		<h3>Внимание! Сайт не предназначен для пользователей младше 16 лет.</h3>
	</div>
	
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>