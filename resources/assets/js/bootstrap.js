
window._ = require('lodash');
window.Vue = require('vue');

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
    window.$ = window.jQuery = require('jquery');

    require('bootstrap-sass');
} catch (e) {}

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

import Echo from 'laravel-echo'

window.Pusher = require('pusher-js');

window.Echo = new Echo({
    broadcaster: 'pusher',
    key: 'a34645dadb342099ceb9',
	cluster: 'eu'
});

$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

window.Echo.channel('objects')
.listen('ObjectCategoryChanged', (e) => {
    //console.log(e.id.length);
    for(var $i=0; $i < e.id.length; $i++) {
        $('#objects-table').append('<tr></tr>'); // Добавляем новую строку в таблицу
        // Заполняем колонки в новой строке
        $('#objects-table > tbody > tr:last').append('<td><input type="radio" value=' + e.id[$i] + ' name="object-radio-in-table"></td>'); 
        $('#objects-table > tbody > tr:last').append('<td>' + e.name[$i] + '</td>');
        $('#objects-table > tbody > tr:last').append('<td>' + e.description[$i] + '</td>');
        $('#objects-table > tbody > tr:last').append('<td><img src=' + e.image[$i] + ' width="100" height="100"></td>');
    }
});

window.Echo.channel('objects')
.listen('WeaponCategoryChanged', (e) => {
    //console.log(e.id.length);
    for(var $i=0; $i < e.id.length; $i++) {
        $('#weapons-table').append('<tr></tr>'); // Добавляем новую строку в таблицу
        // Заполняем колонки в новой строке
        $('#weapons-table > tbody > tr:last').append('<td><input type="radio" value=' + e.id[$i] + ' name="object-radio-in-table"></td>'); 
        $('#weapons-table > tbody > tr:last').append('<td>' + e.description[$i] + '</td>');
        $('#weapons-table > tbody > tr:last').append('<td><img src=' + e.image[$i] + ' width="100" height="100"></td>');
    }
});