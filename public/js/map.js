/*Установка и настройка Яндекс карты*/
    ymaps.ready(init);
    var myMap, myPlacemark;

    function init(){     
        myMap = new ymaps.Map("map-firm", {
            center: [48.71206860, 44.52685286],
            zoom: 17,
            /*controls: ['smallMapDefaultSet']*/
        });

        myMap.behaviors.disable(['drag', 'scrollZoom']);
        myMap.controls.remove('searchControl');
        myMap.controls.remove('searchControl');
        myMap.controls.remove('trafficControl');
        myMap.controls.remove('typeSelector');
        myMap.controls.remove('rulerControl');
        myPlacemark1 = new ymaps.Placemark([48.71206860, 44.52685286],
            {  
                balloonContentHeader: 'Компания "КИЛЛ & МОЛЛ"' 
            }
        );
        
        myMap.geoObjects.add(myPlacemark1);
    }