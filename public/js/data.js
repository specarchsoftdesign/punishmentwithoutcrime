var objectType = document.querySelector("#edit-select-obj");
var weaponType = document.querySelector("#edit-select-gun");
var decorationType = document.querySelector("#edit-select-locale");

$(window).load(function (){
    changeObjectTable();
    changeWeaponTable();
    changeDecorationTable();
});

objectType.onchange = function() {
    changeObjectTable();
};

weaponType.onchange = function(){
    changeWeaponTable();
}

decorationType.onchange = function(){
    changeDecorationTable();
}

function changeObjectTable(){
    var index = objectType.options.selectedIndex;
    if(index == 2){
        document.getElementById("object_price").innerHTML = 'Стоимость: 100000'; 
        // Очищаем таблицу
        $("#objects-table tbody > tr").remove();

        $('#objects-table').append('<tr></tr>'); // Добавляем новую строку в таблицу
        // Заполняем колонки в новой строке
        $('#objects-table > tbody > tr:last').append('<td><input type="radio" value=1 name="object-radio-in-table" checked></td>'); 
        $('#objects-table > tbody > tr:last').append('<td> Путин Владимир Владимирович </td>');
        $('#objects-table > tbody > tr:last').append('<td> Пол: м. Возраст: 65. Рост: 170. Вес: 77. Цвет глаз: светло-карие. Цвет волос: русый. Президент РФ.</td>');
        $('#objects-table > tbody > tr:last').append('<td><img src=storage/company/objects/putin.jpg height="100"></td>'); 
        
        $('#objects-table').append('<tr></tr>'); // Добавляем новую строку в таблицу
        // Заполняем колонки в новой строке
        $('#objects-table > tbody > tr:last').append('<td><input type="radio" value=2 name="object-radio-in-table"></td>'); 
        $('#objects-table > tbody > tr:last').append('<td> Бузова Ольга Игоревна </td>');
        $('#objects-table > tbody > tr:last').append('<td> Пол: ж. Возраст: 31. Рост: 176. Вес: 53. Цвет глаз: темно-карие. Цвет волос: шатенка. Радиоведущая, модель, около певица, актриса.</td>');
        $('#objects-table > tbody > tr:last').append('<td><img src=storage/company/objects/buzova.jpeg height="100"></td>'); 
    }
    else if(index == 1){
        document.getElementById("object_price").innerHTML = 'Стоимость: 20000'; 
        // Очищаем таблицу
        $("#objects-table tbody > tr").remove();
        
        $('#objects-table').append('<tr></tr>'); // Добавляем новую строку в таблицу
        // Заполняем колонки в новой строке
        $('#objects-table > tbody > tr:last').append('<td><input type="radio" value=1 name="object-radio-in-table" checked></td>'); 
        $('#objects-table > tbody > tr:last').append('<td> Кот </td>');
        $('#objects-table > tbody > tr:last').append('<td> Черный дворовый кот. Хромает на заднюю правую лапу. Облезлый. Орет по утрам.</td>');
        $('#objects-table > tbody > tr:last').append('<td><img src=storage/company/objects/cat.jpg height="100"></td>'); 
                
        $('#objects-table').append('<tr></tr>'); // Добавляем новую строку в таблицу
        // Заполняем колонки в новой строке
        $('#objects-table > tbody > tr:last').append('<td><input type="radio" value=2 name="object-radio-in-table"></td>'); 
        $('#objects-table > tbody > tr:last').append('<td> Волк </td>');
        $('#objects-table > tbody > tr:last').append('<td> Серенький волчок, который кусает за бочок. Рост в холке - 85 см. Длина туловища без учета звоста - 150 см. Вес - 70 кг.</td>');
        $('#objects-table > tbody > tr:last').append('<td><img src=storage/company/objects/wolf.jpeg height="100"></td>'); 
    }
    else if(index == 0){
        document.getElementById("object_price").innerHTML = 'Стоимость: 5000'; 
        // Очищаем таблицу
        $("#objects-table tbody > tr").remove();
        
        $('#objects-table').append('<tr></tr>'); // Добавляем новую строку в таблицу
        // Заполняем колонки в новой строке
        $('#objects-table > tbody > tr:last').append('<td><input type="radio" value=1 name="object-radio-in-table" checked></td>'); 
        $('#objects-table > tbody > tr:last').append('<td> Lamborghini Aventador </td>');
        $('#objects-table > tbody > tr:last').append('<td> Спортивный автомоболь. Тип кузова- купе. 2 двери. Руль слева. Объем двигателя - 6498 куб.см. Мощность - 8250 об.мин. Тип привода - постоянный на все колеса. Тип КПП - механика 7 ст. Разгон до 100 км/час - 2,9 с. Длина - 4780 мм. Ширина - 2030 мм. Высота - 1136 мм. Колесная база - 2700 мм. Снаряженная масса - 1575 кг.</td>');
        $('#objects-table > tbody > tr:last').append('<td><img src=storage/company/objects/lamborghini.jpg height="100"></td>'); 
                
        $('#objects-table').append('<tr></tr>'); // Добавляем новую строку в таблицу
        // Заполняем колонки в новой строке
        $('#objects-table > tbody > tr:last').append('<td><input type="radio" value=2 name="object-radio-in-table"></td>'); 
        $('#objects-table > tbody > tr:last').append('<td> IPhone X </td>');
        $('#objects-table > tbody > tr:last').append('<td> Длина - 143,6 мм. Ширина - 70,9 мм. Толщина - 7,7 мм. Вес 174 г.</td>');
        $('#objects-table > tbody > tr:last').append('<td><img src=storage/company/objects/iphone.jpg height="100"></td>'); 
    }
}

function changeWeaponTable(){
    var index = weaponType.options.selectedIndex;
    if(index == 0){
        document.getElementById("weapon_price").innerHTML = 'Стоимость: 15000'; 
        // Очищаем таблицу
        $("#weapons-table tbody > tr").remove();

        $('#weapons-table').append('<tr></tr>'); // Добавляем новую строку в таблицу
        // Заполняем колонки в новой строке
        $('#weapons-table > tbody > tr:last').append('<td><input type="radio" value=1 name="object-radio-in-table" checked></td>'); 
        $('#weapons-table > tbody > tr:last').append('<td>Нож. Длина лезвия 25 см, ширина 5 см. Крупная черная прорезиненная ручка.</td>');
        $('#weapons-table > tbody > tr:last').append('<td><img src=storage/company/weapons/knife.jpg height="100"></td>'); 
        
       
    }
    else if(index == 1){
        document.getElementById("weapon_price").innerHTML = 'Стоимость: 20000'; 
        // Очищаем таблицу
        $("#weapons-table tbody > tr").remove();
        
        $('#weapons-table').append('<tr></tr>'); // Добавляем новую строку в таблицу
        // Заполняем колонки в новой строке
        $('#weapons-table > tbody > tr:last').append('<td><input type="radio" value=2 name="object-radio-in-table" checked></td>'); 
        $('#weapons-table > tbody > tr:last').append('<td> Автомат Калашникова - АК74. Патроны калибра 5,45 мм </td>');
        $('#weapons-table > tbody > tr:last').append('<td><img src=storage/company/weapons/AK47.jpg height="100"></td>'); 
    }
    else if(index == 2){
        document.getElementById("weapon_price").innerHTML = 'Стоимость: 10000'; 
        // Очищаем таблицу
        $("#weapons-table tbody > tr").remove();
        
        $('#weapons-table').append('<tr></tr>'); // Добавляем новую строку в таблицу
        // Заполняем колонки в новой строке
        $('#weapons-table > tbody > tr:last').append('<td><input type="radio" value=3 name="object-radio-in-table" checked></td>'); 
        $('#weapons-table > tbody > tr:last').append('<td>Сюрикэн-звездочка </td>');
        $('#weapons-table > tbody > tr:last').append('<td><img src=storage/company/weapons/sur.jpg height="100"></td>');  
    }
}

    function changeDecorationTable(){
        var index = decorationType.options.selectedIndex;
        if(index == 0){
            document.getElementById("decoration_price").innerHTML = 'Стоимость: 15000'; 
            // Очищаем таблицу
            $("#decorations-table tbody > tr").remove();
    
            $('#decorations-table').append('<tr></tr>'); // Добавляем новую строку в таблицу
            // Заполняем колонки в новой строке
            $('#decorations-table > tbody > tr:last').append('<td><input type="radio" value=1 name="object-radio-in-table" checked></td>'); 
            $('#decorations-table > tbody > tr:last').append('<td>Хвойный лес. Ели закрывают своими густыми кронами небо, и плотно смыкаясь, не пропускают солнечный свет. Под кронами еловых деревьев всегда царят полумрак, влажность и прохлада. Чувствуется насыщенный запах хвои.</td>');
            $('#decorations-table > tbody > tr:last').append('<td><img src=storage/company/decorations/forest.jpg height="100"></td>'); 

            $('#decorations-table').append('<tr></tr>'); // Добавляем новую строку в таблицу
            // Заполняем колонки в новой строке
            $('#decorations-table > tbody > tr:last').append('<td><input type="radio" value=2 name="object-radio-in-table"></td>'); 
            $('#decorations-table > tbody > tr:last').append('<td>Дикий пляж на Черном море. Широкий песчаный берег, вдали виднеются густые заросли лиственных растений. Вода прозрачно-голубая настолько, что видно дно и видно маленьких медуз, которые подплыли к берегу.</td>');
            $('#decorations-table > tbody > tr:last').append('<td><img src=storage/company/decorations/beach.jpg height="100"></td>'); 
        }
        else if(index == 1){
            document.getElementById("decoration_price").innerHTML = 'Стоимость: 20000'; 
            // Очищаем таблицу
            $("#decorations-table tbody > tr").remove();
            
            $('#decorations-table').append('<tr></tr>'); // Добавляем новую строку в таблицу
            // Заполняем колонки в новой строке
            $('#decorations-table > tbody > tr:last').append('<td><input type="radio" value=3 name="object-radio-in-table" checked></td>'); 
            $('#decorations-table > tbody > tr:last').append('<td>Самый дорогой ресторан Санкт-Петербурга - "Дом". В ресторане царит атмосфера девятнадцатого века. Ресторан и правда похож на богатый дом девятнадцатого века, в котором есть библиотека и даже винный погреб.</td>');
            $('#decorations-table > tbody > tr:last').append('<td><img src=storage/company/decorations/restaurant.jpg height="100"></td>'); 

            $('#decorations-table').append('<tr></tr>'); // Добавляем новую строку в таблицу
            // Заполняем колонки в новой строке
            $('#decorations-table > tbody > tr:last').append('<td><input type="radio" value=4 name="object-radio-in-table"></td>'); 
            $('#decorations-table > tbody > tr:last').append('<td>Макдональс. Пластмассовые столы и стулья красного цвета, длинный стол для заказов.Вокруг много мониторов, на которых транслируется реклама.</td>');
            $('#decorations-table > tbody > tr:last').append('<td><img src=storage/company/decorations/mac.jpg height="100"></td>'); 
        }
        else if(index == 2){
            document.getElementById("decoration_price").innerHTML = 'Стоимость: 30000'; 
            // Очищаем таблицу
            $("#decorations-table tbody > tr").remove();
            
            $('#decorations-table').append('<tr></tr>'); // Добавляем новую строку в таблицу
            // Заполняем колонки в новой строке
            $('#decorations-table > tbody > tr:last').append('<td><input type="radio" value=5 name="object-radio-in-table" checked></td>'); 
            $('#decorations-table > tbody > tr:last').append('<td>Эрмитаж. Огромная зала с картинами Ван Гога, высокие сводчатые потолки.</td>');
            $('#decorations-table > tbody > tr:last').append('<td><img src=storage/company/decorations/hermitage.jpg height="100"></td>'); 

            $('#decorations-table').append('<tr></tr>'); // Добавляем новую строку в таблицу
            // Заполняем колонки в новой строке
            $('#decorations-table > tbody > tr:last').append('<td><input type="radio" value=6 name="object-radio-in-table"></td>'); 
            $('#decorations-table > tbody > tr:last').append('<td>Концертный зал. Многочисленные ряды синих бархатных кресел с позолоченными номерками рядов. Огромная сцена, освещенная ярким светом софитов. На сцене много аппаратуры для лазерного шоу.</td>');
            $('#decorations-table > tbody > tr:last').append('<td><img src=storage/company/decorations/concert.jpg height="100"></td>'); 
        }
    }
