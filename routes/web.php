<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/', 'Controller@home');//->middleware(['guest']);//->middleware(['web']);

Route::get('registration', 'Controller@registration');

Route::post('registration', 'AccountController@register');

Route::post('/', [ 'as' => 'login', 'uses' => 'AccountController@logIn']);

Route::get('main', 'Controller@main')->middleware(['auth']);

Route::get('profile', 'ProfileController@showProfile');

Route::get('logout', 'AccountController@logOut');

Route::get('client_history', 'ProfileController@showClientHistory');

Route::get('main_firm', 'Controller@main_firm')->middleware(['auth']);

Route::get('main_police', 'Controller@main_police')->middleware(['auth']);

Route::get('history_orders_firm', 'ProfileController@showHistoryForFirm');

Route::post('history_orders_firm', 'ProfileController@processOrders');

Route::get('order_of_services', 'OrderController@makeOrder');

Route::post('order_of_services', 'OrderController@checkout');

Route::get('objectCategoryChanged', 'OrderController@objectCategoryChanged');

Route::get('history_orders_police', 'ProfileController@showHistoryForPolice');

Route::post('history_orders_police', 'ProfileController@changeAccountStatus');

Route::get('weaponCategoryChanged', 'OrderController@weaponCategoryChanged');

Route::get('setting_police', 'SettingsController@showPoliceSettings');
